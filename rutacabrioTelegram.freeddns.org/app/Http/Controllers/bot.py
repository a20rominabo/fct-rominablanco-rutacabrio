# -*- coding: utf-8 -*-

from telegram.ext import (Updater, CommandHandler)
from telegram import InlineKeyboardButton, InlineKeyboardMarkup
from telegram.ext import CallbackQueryHandler
import datetime 
def start(update, context):
	''' START '''
	# Enviar un mensaje a un ID determinado.
	#context.bot.send_message(update.message.chat_id, "Bienvenido Romina")
	nombre=update.message.chat.first_name
	context.bot.send_message(update.message.chat_id, "Bienvenido :)  "+ nombre+"\n Las opciones son las siguientes: \n /instagram : Enlace a instagram \n /youtube : Enlace a youtube \n /web : Enlace a la web oficial \n Escribe /menu para verlo en formato botones")
def callback_alarm(context):
	context.bot.send_message(chat_id=update.message.chat_id, text='Wait for another 10 Seconds')
def callback_timer(update, context):
	context.bot.send_message(update.message.chat_id,text='Wait for 10 seconds')
	context.job_queue.run_repeating(callback_alarm, 10, context=update.message.chat_id)
def Stop_timer(bot, update, job_queue):
	context.bot.send_message(chat_id=update.message.chat_id,text='Stopped!')
	job_queue.stop()
	# def callback_alarm(context):
# 	context.bot.send_message(chat_id=id, text='Hi, This is a daily reminder')

# def reminder(update,context):
# 	context.bot.send_message(chat_id = update.effective_chat.id , text='Daily reminder has been set! You\'ll get notified at 8 AM daily')
# 	# context.job_queue.run_daily(callback_alarm, context=update.message.chat_id,days=(0, 1, 2, 3, 4, 5, 6),time =datetime.time(hour = 15, minute = 36, second = 20))
# 	context.job_queue.run_custom(callback_alarm,)
def menu(update, context):
    # texto="menu\ninstagram\nyoutube"
    
	keyboard = [[InlineKeyboardButton("Instagram", callback_data='Instagram'),InlineKeyboardButton("Youtube", callback_data='Youtube')],[InlineKeyboardButton("Web oficial", callback_data='Web')]]
	reply_markup = InlineKeyboardMarkup(keyboard)
	update.message.reply_text('Por favor escoja una opción:', reply_markup=reply_markup)
	#context.bot.send_message(update.message.chat_id, keyboard)
def button(update, context):
    query = update.callback_query
    a = 'Instagram'
    b = 'Youtube'
    url="https://www.instagram.com/rutacabrio/?hl=es"
    url2="https://www.youtube.com/channel/UCw5r5MxUVnCNb7kZAwO7yaA"
    url3="http://rutacabrio.ddnsfree.com/"
    if query == a :
        context.bot.editMessageText(text="Ha escogido ver	%s : %s" % (query.data, url),
                        chat_id=query.message.chat_id,
                        message_id=query.message.message_id)
    elif query == b :
        context.bot.editMessageText(text="Ha escogido ver	%s : %s" % (query.data, url2),
                        chat_id=query.message.chat_id,
                        message_id=query.message.message_id)
    else :
        context.bot.editMessageText(text="Ha escogido ver	%s : %s" % (query.data, url3),
                        chat_id=query.message.chat_id,
                        message_id=query.message.message_id)			
def instagram(update, context):     
	url="https://www.instagram.com/rutacabrio/?hl=es"
	context.bot.send_message(update.message.chat_id, url)
def youtube(update, context):     
	url="https://www.youtube.com/channel/UCw5r5MxUVnCNb7kZAwO7yaA"
	context.bot.send_message(update.message.chat_id, url)
def web(update, context):     
	url="http://rutacabrio.ddnsfree.com/"
	context.bot.send_message(update.message.chat_id, url)
def main():
	TOKEN="5182539600:AAGqVBp3NpnNH7KDY-M9PlEqARWgCVNIkzc"
	updater=Updater(TOKEN, use_context=True)
	dp=updater.dispatcher

	# Eventos que activarán nuestro bot.
	dp.add_handler(CommandHandler('start',	start))
	dp.add_handler(CommandHandler('menu',	menu))
	dp.add_handler(CommandHandler('instagram',	instagram))
	dp.add_handler(CommandHandler('youtube',	youtube))
	dp.add_handler(CommandHandler('web',	web))
	# dp.add_handler(CommandHandler('hora',	reminder, pass_job_queue=True))
	dp.add_handler(CallbackQueryHandler(button))
	dp.add_handler(CommandHandler('inicio', callback_timer, pass_job_queue=True))
	dp.add_handler(CommandHandler('stop', Stop_timer, pass_job_queue=True))
	
	# Comienza el bot
	updater.start_polling()
	# Lo deja a la escucha. Evita que se detenga.
	updater.idle()

if __name__ == '__main__':
	main()

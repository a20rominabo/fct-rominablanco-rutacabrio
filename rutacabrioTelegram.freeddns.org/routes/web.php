<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\BotController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//https://rutacabrioTelegram.freeddns.org/xxxsecretxxx29349asdfj3rutacabrio357
//Route::match(['get', 'post'], '/xxxsecretxxx29349asdfj3rutacabrio357', [BotController::class, 'gestionar']);

//  Route::get('/', function () {
//      return view('welcome');
//  });

//Route::match(['get', 'post'], '/xxxsecretxxx29349asdfj3rutacabrio357', [BotController::class, 'index']);
Route::get('/',[BotController::class,'index']);
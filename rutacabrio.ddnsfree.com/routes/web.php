<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\RutacabrioController;
use App\Http\Controllers\PostcomunidadController;
use App\Http\Controllers\PostrutacabrioController;
use App\Http\Controllers\ComentariocomunidadController;
use App\Http\Controllers\ComentariorutacabrioController;
use App\Http\Controllers\ComentarioPerfilController;
use App\Http\Controllers\ComentariocomPerfilController;
use App\Http\Controllers\LogoutControllerController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('inicio');
Route::get('/contacto', function () {
    return view('contacto');
})->name('contacto');
Route::get('/politica', function () {
    return view('policy');
})->name('policy');
Route::get('/sesion', function () {
    return view('auth.login');
})->name('session');
Route::get('/registro', function () {
    return view('auth.register');
})->name('registrarse');
Route::get('/sesion/inicio', function () {
    return view('sesionIniciada');
})->name('sessionInicio');
Route::post('/cerrar', function () {
    auth()->logout();
    return view('welcome');
})->name('logout');

 Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
     return view('sesionIniciada');
 });
//->name('dashboard')
Route::get('/postruta/crear',function(){
    return view('crearRuta');
})->name('crearcabrio');



Route::resource ('postrutacabrio',PostrutacabrioController::class);
Route::resource ('rutacabrio',RutacabrioController::class);
Route::resource ('comentariorutacabrio',ComentariorutacabrioController::class);
Route::resource ('comentariocomunidad',ComentariocomunidadController::class);
Route::resource ('comunidadpost',PostcomunidadController::class);
Route::resource ('comentarioperfil',ComentarioPerfilController::class);
Route::resource ('comentarioperfilcomunidad',ComentariocomPerfilController::class);

Route::get('/postruta',[PostrutacabrioController::class,'index'])->name('postruta');
Route::get('/postruta/2021',[PostrutacabrioController::class,'show2021'])->name('mostrar2021');
Route::get('/postruta/2022',[PostrutacabrioController::class,'show2022'])->name('mostrar2022');
Route::get('/postruta/2020',[PostrutacabrioController::class,'show2020'])->name('mostrar2020');
Route::get('/comunidadpost',[PostcomunidadController::class,'index'])->name('postcomunidad');
Route::get('/postruta/{ruta}',[PostrutacabrioController::class,'showcoment'])->name('contenido');
Route::get('/postcomunidad/{ruta}',[PostcomunidadController::class,'show1'])->name('mostrar');


Route::get('/sesion/perfil/comentario',[ComentarioPerfilController::class,'index'])->name('perfilcomentario')->middleware('auth');
Route::get('/sesion/perfil/comentario/editar',[ComentarioPerfilController::class,'edit'])->name('actualizarcomentario')->middleware('auth');



Route::get('/sesion/perfil/post',[RutacabrioController::class,'listarpost'])->name('perfilpost')->middleware('auth');
Route::get('/sesion/perfil/post/crear',[RutacabrioController::class,'crearindex'])->name('crear')->middleware('auth');
Route::post('/sesion/perfil/post/crear',[RutacabrioController::class,'poststore'])->name('enviar')->middleware('auth');

Route::get('/sesion/perfil/postscomunidad',[RutacabrioController::class,'listarpostcomunidad'])->name('perfilpostcomunidad')->middleware('auth');

Route::get('/sesion/perfil/ruta',[RutacabrioController::class,'listarruta'])->name('perfilruta')->middleware('auth');

Route::get('/sesion/perfil/ruta/crear',[RutacabrioController::class,'rutacrear'])->name('crearruta')->middleware('admin');
Route::post('/sesion/perfil/ruta/crear',[RutacabrioController::class,'rutastore'])->name('enviarruta')->middleware('admin');
Route::get('/sesion/perfil/ruta/{publicacion}',[RutacabrioController::class,'showruta'])->name('mostrarruta')->middleware('admin');
Route::delete('/sesion/perfil/ruta/{publicacion}',[RutacabrioController::class,'destroyruta'])->name('destruirpost')->middleware('admin');
Route::get('/sesion/perfil/ruta/editar/{publicacion}',[RutacabrioController::class,'editruta'])->name('editarrutacabrio')->middleware('admin');
Route::put('/sesion/perfil/ruta/editar/{publicacion}',[RutacabrioController::class,'updateruta'])->name('actualizarruta')->middleware('admin');

Route::get('/sesion/perfil/usuario',[RutacabrioController::class,'listarusuarios'])->name('listarusuarios')->middleware('admin');
Route::get('/sesion/perfil/usuario/editar/{publicacion}',[RutacabrioController::class,'editarusuarios'])->name('editarusuarios')->middleware('admin');
Route::put('/sesion/perfil/usuario/editar/{publicacion}',[RutacabrioController::class,'updateusuarios'])->name('actualizarusuarios')->middleware('admin');
// Route::post('/sesion/perfil/usuario/editar/{publicacion}',[RutacabrioController::class,'destroyusuarios'])->name('borrarusuarios')->middleware('admin');

Route::get('/sesion/perfil/{id}',[RutacabrioController::class,'perfiledit'])->name('editarperfil')->middleware('auth');
Route::put('/sesion/perfil/{idp}',[RutacabrioController::class,'perfilup'])->name('modificar')->middleware('auth');
Route::get('/sesion/perfil',[RutacabrioController::class,'index'])->name('perfil')->middleware('auth');


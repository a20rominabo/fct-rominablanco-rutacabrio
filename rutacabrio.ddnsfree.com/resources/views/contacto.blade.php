

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport"    content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author"      content="Romina Blanco Ortega">
	
	<title>Ruta Cabrio</title>

	<link rel="shortcut icon" href="images/r.png">
	
	<!-- Bootstrap itself -->
	<link href="plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">

	<!-- Custom styles -->
	<link rel="stylesheet" href="css/magister.css">

	<!-- Fonts -->
	<link href="css/awesome.css" rel="stylesheet" type="text/css">
	<link href='css/google.css' rel='stylesheet' type='text/css'>
</head>

<!-- use "theme-invert" class on bright backgrounds, also try "text-shadows" class -->
<body class="theme-invert">



<!-- (Contact) section -->

	<div class="container">
	
		<h2 class="text-center title"> Contacto </h2>

		<div class="row">
			<div class="col-sm-8 col-sm-offset-2 text-center">
				<p class="lead">Para cualquier error o informacion sobre la página, el contenido, o cualquier idea o duda, pongase en contacto con : </p>
				<p><b>rutacabrio@template.com</b><br><br></p>
				
			</div>
		</div>
		<div class="row">
			<div class="col-sm-8 col-sm-offset-2 text-center">
				<a href="{{route('inicio')}}"><b>Inicio</b></a>
				
			</div>
		</div>

		

	</div>



<!-- Load js libs only when the page is loaded. -->
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.js"></script>
<script src="js/modernizr.custom.72241.js"></script>
<!-- Custom template scripts -->
<script src="js/magister.js"></script>
</body>
</html>
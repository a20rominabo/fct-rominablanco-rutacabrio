@extends('plantillaInicioRutas')
@section('sectionHeader')
<h1>Ruta cabrio</h1>
@stop
@section('sectionMenu')					
<!-- RUTACABRIO -->
	<li class="dropdown ">
		<a href="{{route('postruta')}}">Rutacabrio</a>
	</li><!-- / RUTACABRIO -->
<!-- COMUNIDAD  -->
	<li class="dropdown ">
		<a href="{{route('postcomunidad')}}">Comunidad</a>
	</li><!-- / COMUNIDAD -->
@stop
@section('sectionPageHeader')
<h1 class="page-name"> {{$ruta->titulo}}</h1>
	<ol class="breadcrumb">
		<li><a href="{{route('postruta')}}">Rutacabrio</a></li>
		<li class="active">Post : {{$ruta->titulo}}</li>
	</ol>
@stop
@section('contenido')				
@include('plantillas.contenidoblogcabrio')
@stop
	
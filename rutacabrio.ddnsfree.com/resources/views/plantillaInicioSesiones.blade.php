<!DOCTYPE html>
<html lang="en">
<head>

  <!-- Basic Page Needs
  ================================================== -->
  <meta charset="utf-8">
  <title>RutaCabrio</title>

  <!-- Mobile Specific Metas
  ================================================== -->
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="description" content="Construction Html5 Template">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=5.0">
  <meta name="author" content="Romina Blanco Ortega">
  <meta name="generator" content="">
  
  <!-- Favicon -->
	<link rel="shortcut icon" type="image/x-icon" href="{{asset ('/images/r.png')}}" />

<!-- Themefisher Icon font -->
<link rel="stylesheet" href="{{asset ('/plugins/themefisher-font/style.css')}}">
<!-- bootstrap.min css -->
<link rel="stylesheet" href="{{asset ('/plugins/bootstrap/css/bootstrap.min.css')}}">

<!-- Animate css -->
<link rel="stylesheet" href="{{asset ('/plugins/animate/animate.css')}}">
<!-- Slick Carousel -->
<link rel="stylesheet" href="{{asset ('/plugins/slick/slick.css')}}">
<link rel="stylesheet" href="{{asset ('/plugins/slick/slick-theme.css')}}">

<!-- Main Stylesheet -->
<link rel="stylesheet" href="{{asset ('/css/style.css')}}">

</head>

<body id="body">
    <!-- Start Top Header Bar -->
<section class="top-header">
	<div class="container">
		<div class="row">
			
			<div class="col-md-12 col-xs-12 col-sm-12">
				<!-- Site Logo -->
				<div class="logo text-center">
                @yield('sectionHeader')
                </div>
		    </div>
			
		</div>
	</div>
</section><!-- End Top Header Bar -->

<!-- Main Menu Section -->
<section class="menu">
	<nav class="navbar navigation">
		<div class="container">
			<div class="navbar-header">
				<h2 class="menu-title">Menu</h2>
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
					aria-expanded="false" aria-controls="navbar">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>

			</div><!-- / .navbar-header -->

			<!-- Navbar Links -->
			<div id="navbar" class="navbar-collapse collapse text-center">
				<ul class="nav navbar-nav">

					<!-- INICIO -->
					<li class="dropdown ">
						<a href="{{route('inicio')}}">Inicio</a>
					</li><!-- / INICIO -->
					<!-- Pages -->
					<li class="dropdown full-width dropdown-slide">
							<a href="#!" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="350"
							role="button" aria-haspopup="true" aria-expanded="false"> Contenido <span
								class="tf-ion-ios-arrow-down"></span></a>
						<div class="dropdown-menu">
							<div class="row">
								@if((auth()->user()->categoria)=== 'usuariorutacabrio')
								<!-- Introduction -->
								<div class="col-sm-3 col-xs-12">
									<ul>
										<li class="dropdown-header">Publicaciones</li>
										<li role="separator" class="divider"></li>
										<li><a href="{{route('perfilruta')}}">Post</a></li>
										<li><a href="{{route('perfilcomentario')}}">Comentarios</a></a></li>									
									</ul>
								</div>
								<!-- Contact -->
								<div class="col-sm-3 col-xs-12">
									<ul>
									<li  class="dropdown-header">Perfil</li>
									<li role="separator" class="divider"></li>
									<li><a href="{{route('perfil')}}">Mi Perfil Cabrio</a></li>
									</ul>
								</div>
								@else
								<!-- Introduction -->
								<div class="col-sm-3 col-xs-12">
									<ul>
										<li class="dropdown-header">Publicaciones</li>
										<li role="separator" class="divider"></li>
										<li><a href="{{route('perfilpost')}}">Post</a></li>
										<li><a href="{{route('perfilcomentario')}}">Comentarios</a></a></li>									
									</ul>
								</div>
								<!-- Contact -->
								<div class="col-sm-3 col-xs-12">
									<ul>
									@if((auth()->user()->categoria)=== 'usuariorutacabrio')
									<li  class="dropdown-header">Perfil</li>
										<li role="separator" class="divider"></li>
										<li><a href="{{route('perfil')}}">Mi Perfil Cabrio</a></li>
									@else
										<li  class="dropdown-header">Perfil</li>
										<li role="separator" class="divider"></li>
										<li><a href="{{route('perfil')}}">Ver Perfil</a></li>
									@endif
									</ul>
								</div>
								@endif
							</div><!-- / .row -->
						</div><!-- / .dropdown-menu -->
					</li><!-- / Pages -->
					
                    @yield('sectionMenu')

                    <!-- Comunidad -->
					<li class="dropdown ">
						<a href="{{route('postcomunidad')}}">Comunidad</a>
					</li><!-- / Comunidad -->
					<!-- session start-->
					@if((auth()->user()->categoria)=== 'usuariorutacabrio')
					<li class="dropdown ">
						<a href="{{route('perfil')}}">Ver perfil Rutacabrio</a>
					</li>
					@else
					<li class="dropdown ">
						<a href="{{route('perfil')}}">Ver perfil</a>
					</li><!-- / session  start-->
					@endif
					<!-- session -->
					<li class="dropdown ">
						<a class="nav-link" href="#" onclick="event.preventDefault();document.getElementById('logout-form').submit();">Desconectar</a>
					</li><!-- / session -->
			</div>
			<!--/.navbar-collapse -->
		</div><!-- / .container -->
	</nav>
</section>
<section class="page-header">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="content ">
                @yield('sectionPageHeader')
				@if((auth()->user()->categoria)=== 'usuariorutacabrio')
				@yield('usuarioHeader')
				@else
				@yield('usuarioHeader2')
				@endif	
				@yield('central')
                </div>
			</div>
		</div>
	</div>
</section>

@yield('contenido')

<form method="POST" id="logout-form" action="{{ route('logout') }}">
        @csrf
</form>
<footer class="footer section text-center">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<ul class="social-media">
					
					<li>
						<a href="https://www.instagram.com/rutacabrio/">
							<i class="tf-ion-social-instagram"></i>
						</a>
					</li>
					
					
				</ul>
				<ul class="footer-menu text-uppercase">
					<li>
						<a href="{{route('contacto')}}">CONTACTO</a>
					</li>
					
				</ul>
				<p class="copyright-text">Copyright &copy;2021, Designed &amp; Developed by <a href="">cousiñas</a></p>
			</div>
		</div>
	</div>
</footer>

    <!-- 
    Essential Scripts
    =====================================-->
    
    <!-- Main jQuery -->
    <script src="{{asset ('plugins/jquery/dist/jquery.min.js')}}"></script>
    <!-- Bootstrap 3.1 -->
    <script src="{{asset ('plugins/bootstrap/js/bootstrap.min.js')}}"></script>
    <!-- Bootstrap Touchpin -->
    <script src="{{asset ('plugins/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js')}}"></script>
    <!-- Instagram Feed Js -->
    <script src="{{asset ('plugins/instafeed/instafeed.min.js')}}"></script>
    <!-- Video Lightbox Plugin -->
    <script src="{{asset ('plugins/ekko-lightbox/dist/ekko-lightbox.min.js')}}"></script>
    <!-- Count Down Js -->
    <script src="{{asset ('plugins/syo-timer/build/jquery.syotimer.min.js')}}"></script>

    <!-- slick Carousel -->
    <script src="{{asset ('plugins/slick/slick.min.js')}}"></script>
    <script src="{{asset ('plugins/slick/slick-animation.min.js')}}"></script>

    

    <!-- Main Js File -->
    <script src="js/script.js"></script> 
    


  </body>
  </html>


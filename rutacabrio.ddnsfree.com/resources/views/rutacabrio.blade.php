@extends('plantillaInicioRutas')
@section('sectionHeader')
<h1>Ruta cabrio</h1>			
@stop
@section('sectionMenu')
<!-- COMUNIDAD -->
	<li class="dropdown ">
		<a href="{{route('postcomunidad')}}">Comunidad</a>
	</li><!-- / COMUNIDAD -->
 <!-- Pages -->
 <li class="dropdown full-width dropdown-slide">
	<a href="#!" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="350"
							role="button" aria-haspopup="true" aria-expanded="false"> Contenido <span
								class="tf-ion-ios-arrow-down"></span></a>
		<div class="dropdown-menu">
			<div class="row ">

				<!-- Introduction -->
				<div class="col-sm-12 col-xs-12">
					<ul class="text-center" >
						<li class="dropdown-header ">Años</li>
						<li role="separator" class="divider"></li>
						<li><a href="{{route('mostrar2022')}}">2017</a></li>
						<li ><a href="{{route('mostrar2021')}}">2018</a></li>
						<!-- <li ><a href="{{route('mostrar2020')}}">2020</a></li> -->
					</ul>
				</div>				
			</div><!-- / .row -->
		</div><!-- / .dropdown-menu -->
</li><!-- / Pages -->
@stop
@section('sectionPageHeader')
<h1 class="page-name">Rutacabrio</h1>
	<ol class="breadcrumb">
		<li><a href="{{route('inicio')}}">Inicio</a></li>
			<li class="active">Rutacabrio</li>
	</ol>
@stop			
@section('contenido')
@include('plantillas.contenidoruta') 
@stop



<x-guest-layout>
    <x-jet-authentication-card>
        <x-slot name="logo">
          
        </x-slot>

        <x-jet-validation-errors class="mb-3" />

        <div class="card-body">
            <form method="POST" action="{{ route('register') }}">
                @csrf

                <div class="mb-3">
                    <x-jet-label value="{{ __('Nombre') }}" />

                    <x-jet-input class="{{ $errors->has('nombre') ? 'is-invalid' : '' }}" type="text" name="nombre"
                                 :value="old('nombre')" required autofocus autocomplete="nombre" />
                    <x-jet-input-error for="nombre"></x-jet-input-error>
                </div>

                <div class="mb-3">
                    <x-jet-label value="{{ __('Apellidos') }}" />

                    <x-jet-input class="{{ $errors->has('apellidos') ? 'is-invalid' : '' }}" type="text" name="apellidos"
                                 :value="old('apellidos')" required autofocus autocomplete="apellidos" />
                    <x-jet-input-error for="apellidos"></x-jet-input-error>
                </div>

                <div class="mb-3">
                    <x-jet-label value="{{ __('Login') }}" />

                    <x-jet-input class="{{ $errors->has('login') ? 'is-invalid' : '' }}" type="text" name="login"
                                 :value="old('login')" required autofocus autocomplete="login" />
                    <x-jet-input-error for="login"></x-jet-input-error>
                </div>

                <div class="mb-3">
                    <x-jet-label value="{{ __('Categoria') }}" style="display:none"/>

                    <x-jet-input  type="text" name="categoria"
                                 value="NINGUNA"   style="display:none"/>
                    <x-jet-input-error for="categoria"></x-jet-input-error>
                </div>

                 <div class="mb-3">
                    <x-jet-label value="{{ __('Telefono') }}" />

                    <x-jet-input class="{{ $errors->has('telefono') ? 'is-invalid' : '' }}" type="text" name="telefono"
                                 :value="old('telefono')" required autofocus autocomplete="telefono" />
                    <x-jet-input-error for="telefono"></x-jet-input-error>
                </div>

                <div class="mb-3">
                    <x-jet-label value="{{ __('Tipo Coche (puede ser familiar, berlina,cabrio...)') }}" />

                    <x-jet-input class="{{ $errors->has('tipoCoche') ? 'is-invalid' : '' }}" type="text" name="tipoCoche"
                                 :value="old('tipoCoche')" required autofocus autocomplete="tipoCoche" />
                    <x-jet-input-error for="tipoCoche"></x-jet-input-error>
                </div>

                <div class="mb-3">
                    <x-jet-label value="{{ __('Email') }}" />

                    <x-jet-input class="{{ $errors->has('email') ? 'is-invalid' : '' }}" type="email" name="email"
                                 :value="old('email')" required />
                    <x-jet-input-error for="email"></x-jet-input-error>
                </div>

                <div class="mb-3">
                    <x-jet-label value="{{ __('Password') }}" />

                    <x-jet-input class="{{ $errors->has('password') ? 'is-invalid' : '' }}" type="password"
                                 name="password" required autocomplete="new-password" />
                    <x-jet-input-error for="password"></x-jet-input-error>
                </div>

                <div class="mb-3">
                    <x-jet-label value="{{ __('Confirm Password') }}" />

                    <x-jet-input class="form-control" type="password" name="password_confirmation" required autocomplete="new-password" />
                </div>

                @if (Laravel\Jetstream\Jetstream::hasTermsAndPrivacyPolicyFeature())
                    <div class="mb-3">
                        <div class="custom-control custom-checkbox">
                            <x-jet-checkbox id="terms" name="terms" />
                            <label class="custom-control-label" for="terms">
                                {!! __('I agree to the :terms_of_service and :privacy_policy', [
                                            'terms_of_service' => '<a target="_blank" href="'.route('terms.show').'">'.__('Terms of Service').'</a>',
                                            'privacy_policy' => '<a target="_blank" href="'.route('policy.show').'">'.__('Privacy Policy').'</a>',
                                    ]) !!}
                            </label>
                        </div>
                    </div>
                @endif

                <div class="mb-0">
                    <div class="d-flex justify-content-end align-items-baseline">
                        <a class="text-muted me-3 text-decoration-none" href="{{ route('login') }}">
                            {{ __('Already registered?') }}
                        </a>

                        <x-jet-button>
                            {{ __('Register') }}
                        </x-jet-button>
                    </div>
                </div>
            </form>
        </div>
    </x-jet-authentication-card>
</x-guest-layout>
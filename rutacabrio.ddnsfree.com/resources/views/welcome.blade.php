<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport"    content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author"      content="Romina Blanco Ortega">
	
	<title>Ruta Cabrio</title>

	<link rel="shortcut icon" href="images/r.png">
	
	<!-- Bootstrap itself -->
	<link href="plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
	
	<!-- Custom styles -->
	<link rel="stylesheet" href="css/magister.css">

	<!-- Fonts -->
	<link href="css/awesome.css" rel="stylesheet" type="text/css">
	<link href='css/google.css' rel='stylesheet' type='text/css'>
</head>

<!-- use "theme-invert" class on bright backgrounds, also try "text-shadows" class -->
<body class="theme-invert">

<nav class="mainmenu">
	<div class="container">
		<div class="dropdown">
			<button type="button" class="navbar-toggle" data-toggle="dropdown"><span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
			<!-- <a data-toggle="dropdown" href="#">Dropdown trigger</a> -->
			<ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
				<li><a href="#head" class="active">Inicio</a></li>
				<li><a href="#about">Sobre mí</a></li>
				<li><a href="#themes">Rutas</a></li>
				<li><a href="#contact">Contacto</a></li>
			</ul>
		</div>
	</div>
</nav>


<!-- Main (Home) section -->
<section class="section" id="head">
	<div class="container escoge">

		<div class="row">
			<div class="col-md-10 col-lg-10 col-md-offset-1 col-lg-offset-1 text-center">	

				<!-- Site Title, your name, HELLO msg, etc. -->
				
				<h1 class="title" style="color:white;font-weight: 500;">Ruta Cabrio</h1> 
				
				
				<h2 class="subtitle" style="color:black"> </h2>
				
				
				<!-- <div class="caja2" >
				<h1 id="titulo" class="title">Ruta Cabrio</h1>
				</div> -->
				<!-- Short introductory (optional) -->
				<h3 class="tagline" style="color:white; padding-top:35px; ">
					Podrás descubrir rutas y vistas fascinantes
				</h3>
				
				<!-- Nice place to describe your site in a sentence or two -->
				<p><a href="{{route('postruta')}}" class="btn btn-default btn-lg " style="color:white">Descúbrelo ... </a></p>
	
			</div> <!-- /col -->
		</div> <!-- /row -->
	
	</div>
</section>

<!-- Second (About) section -->
<section class="section" id="about">
	<div class="container escoge">
	
		<h2 class="text-center title">Sobre mí</h2>
		<div class="row">
			<div class="col-sm-4 col-sm-offset-2">    
				<h4 style="-webkit-text-fill-color: white;-webkit-text-stroke: 0.05px white; "><strong>Por qué crearlo?<br></strong></h4>
				<p style="color:white;">Porque me gusta viajar, descubrir e innovar. Con estas rutas no solo disfrutarás conduciendo por sus carreteras sino también sus impresionantes vistas.</p>    
				<h4 style="-webkit-text-fill-color: white;-webkit-text-stroke: 0.05px white; " ><strong>Links<br></strong></h4>    
				<p class="h4" style="color:white;"><a style="color:white;" href="https://www.instagram.com/rutacabrio/?hl=es"><strong>Instagram</strong></a> / <a style="color:white;" href="https://www.youtube.com/channel/UCw5r5MxUVnCNb7kZAwO7yaA"><strong>Youtube</strong></a> </p>
			</div>
			<div class="col-sm-4">
				<h4 style="-webkit-text-fill-color: white;-webkit-text-stroke: 0.05px white; "><strong>Buscando adictos<br></strong></h4>    
				<p style="color:white;">Decidí crear un apartado en el cual la gente pueda aportar nuevas experiencias. Donde se pueda generar más rutas y encontrar sitio únicos en todo el mundo</p>    
				
			</div>
		</div>
	</div>
</section>

<!-- Third (Works) section -->
<section class="section" id="themes">
	<div class="container escoge">
	
		<h2 class="text-center title" style="">---   Escoge   ---</h2>

		<div class="row">
			<div class="col-sm-8 col-sm-offset-2 text-center">
				  <ul class="list-inline list-social">
					<li><a style="color:white;" href="{{route('postruta')}}" class="btn btn-link"> Ruta Cabrio</a></li>
					<li><a style="color:white;" href="{{route('postcomunidad')}}" class="btn btn-link"> Comunidad </a></li>
					
				</ul>
			</div>
		</div>

	</div>
</section>

<!-- Fourth (Contact) section -->
<section class="section" id="contact">
	<div class="container escoge">
	
		<h2 class="text-center title"> Contacto </h2>

		<div class="row">
			<div class="col-sm-8 col-sm-offset-2 text-center">
				<p class="lead" style="color:white;">Para cualquier error o informacion sobre la página, el contenido, o cualquier idea o duda, pongase en contacto con : </p>
				<p class="lead email" style="-webkit-text-fill-color: white;-webkit-text-stroke: 0.05px white; "><b>rutacabrio@template.com</b><br><br></p>
				
			</div>
		</div>

	</div>
</section>


<!-- Load js libs only when the page is loaded. -->
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.js"></script>
<script src="js/modernizr.custom.72241.js"></script>
<!-- Custom template scripts -->
<script src="js/magister.js"></script>
</body>
</html>


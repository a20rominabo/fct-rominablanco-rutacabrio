@extends('plantillaInicioRutas')
@section('sectionHeader')
<h1>Ruta cabrio Comunidad</h1>		
@stop
@section('sectionMenu')
<!-- rutacabrio -->
<li class="dropdown ">
	<a href="{{route('postruta')}}">Rutacabrio</a>
</li><!-- / rutacabrio -->
@stop	


@section('sectionPageHeader')
@auth
<h1 class="page-name">Comunidad</h1>
	<ol class="breadcrumb">
		<li><a href="{{route('inicio')}}">Inicio</a></li>
		<li class="active">Comunidad</li>
	</ol>
@endauth
@stop
@section('contenido')
@auth
<div class="page-wrapper">
	<div class="container">
		<div class="row">
		 	@foreach($postcomunidades as $ruta)
			<div class="col-md-6">
		        <div class="post" style="border:none">
		        	<div class="post-thumb paginar">
		            	<img class="img-responsive paginar" src='{{URL::asset("/storage/$ruta->foto")}}' alt="">
		            </div>
		          	<h2 class="post-title">{{$ruta->titulo}}</h2>
		          	<div class="post-meta">
		            	<ul>
		              		<li>
		                		<i class="tf-ion-ios-calendar"></i> {{$ruta->fecha}}
		              		</li>
		              		<li>
		                		<i class="tf-ion-ios-pricetags"></i> {{$ruta->comunidadAutonoma}}, {{$ruta->ciudad}}
		              		</li>		              
		            		</ul>
		          	</div>
		          	<div class="post-content">
		            	<a href="{{route('mostrar',$ruta)}}" class="btn btn-main">Seguir leyendo</a> 
					</div>
				</div>
        	</div>
			@endforeach	
		</div>
	</div>
</div>	     		

<div class="text-center">
			{!! $postcomunidades->links()!!}
			
</div>
<!-- <footer class="footer section text-center">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<ul class="social-media">
					
					<li>
						<a href="https://www.instagram.com/rutacabrio/">
							<i class="tf-ion-social-instagram"></i>
						</a>
					</li>
					
					
				</ul>
				<ul class="footer-menu text-uppercase">
					<li>
						<a href="{{route('contacto')}}">CONTACTO</a>
					</li>
					
				</ul>
				<p class="copyright-text">Copyright &copy;2021, Designed &amp; Developed by <a href="">cousiñas</a></p>
			</div>
		</div>
	</div>
</footer> -->
@endauth
@guest  
<section class="page-header">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="content">
					<h1 class="page-name">Para ver este contenido hay que estar registrado</h1>
					<ol class="breadcrumb">
						<li><a href="{{route('postruta')}}">Volver a rutacabrio</a></li>
						
					</ol>
				</div>
			</div>
		</div>
	</div>
</section>

@endguest
<footer class="footer section text-center comunidad">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<ul class="social-media">
					
					<li>
						<a href="https://www.instagram.com/rutacabrio/">
							<i class="tf-ion-social-instagram"></i>
						</a>
					</li>
					
					
				</ul>
				<ul class="footer-menu text-uppercase">
					<li>
						<a href="{{route('contacto')}}">CONTACTO</a>
					</li>
					
				</ul>
				<p class="copyright-text">Copyright &copy;2021, Designed &amp; Developed by <a href="">cousiñas</a></p>
			</div>
		</div>
	</div>
</footer>
@stop





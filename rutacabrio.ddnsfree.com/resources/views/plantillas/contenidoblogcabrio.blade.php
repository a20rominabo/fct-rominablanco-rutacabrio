<section class="page-wrapper">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="post-single">
					<div class="post-thumb">
					@if($ruta->foto != null)
					<img class="img-responsive" src='{{URL::asset("/storage/$ruta->foto")}}' alt="">
					@elseif($ruta->video != null)
					<div class="embed-responsive embed-responsive-16by9">

  						<iframe class="embed-responsive-item" src='{{URL::asset("/storage/$ruta->video")}}' sandbox></iframe>
					</div>	
					@endif
				</div>
					
					<h2 class="post-title">{{$ruta->titulo}}</h2>
					<div class="post-meta">
						<ul>
							<li>
								<i class="tf-ion-ios-calendar"></i>{{ \Carbon\Carbon::parse($ruta->fecha)->format('d/m/Y') }}
							</li>
							<li>
								<i class="tf-ion-android-person"></i> Escrito por Rutacabrio
							</li>
							<li>
								<a href="#!"><i class="tf-ion-ios-pricetags"></i>{{$ruta->comunidadAutonoma}}</a>,<a href="#!">{{$ruta->ciudad}}</a>
							</li>
							<li>
								<a href="#!"><i class="tf-ion-chatbubbles"></i>{{$comentarios}}  comentarios</a>
							</li>
						</ul>
					</div>
					<div class="post-content post-excerpt">
						<p>{{$ruta->descripcion}}</p>
						<blockquote class="quote-post">
				            <a href="{{$ruta->enlacesExternos}}">{{$ruta->enlacesExternos}}</a>
				        </blockquote>
				        <img src='' width="320" /><br />
				        
				    </div>
					<div class="contact-details col-md-12 " >
						<div class="google-map">
							<div id="map" data-value="{{$ruta->titulo}}">
							<div id="latitud" data-value="{{$ruta->coordenadaLatitud}}"></div>
							<div id="longitud" data-value="{{$ruta->coordenadaLongitud}}"></div>
							</div>
						</div>
						<ul class="contact-short-info" >
							<li>
								<!-- tf-ion-android-globe -->
								<i class="tf-map2">  {{$ruta->coordenadaLatitud}} , {{$ruta->coordenadaLongitud}} </i>
								<span></span>
							</li>
						</ul>		
					</div>
				    <div class="post-social-share">
				        <h3 class="post-sub-heading">Ver este Post en Instagram</h3>
				        <div class="social-media-icons">
				        	<ul>								
								<li><a class="instagram" href="{{$ruta->enlaceInstagram}}"><img src="{{asset ('/images/instagram1.png')}}" style="width:55px;height:55px;"></a></li>							
							</ul>
				        </div>
				    </div>				
					
				    <div class="post-comments">
				    	<h3 class="post-sub-heading">{{$comentarios}} Comentarios</h3>

						
				    	<ul class="media-list comments-list m-bot-50 clearlist">
							@foreach($publicaciones as $publicado)				
						   <!-- Comment Item start-->
						    <li class="media">
						        <div class="media-body">
						            <div class="comment-info">
						                <div class="comment-author">
						                    <!-- <a href="#!"></a> -->
											<p>{{$publicado->nick}}</p>
						                </div>
						                <time>{{\Carbon\Carbon::parse($publicado->fecha)->format('d/m/Y')}}</time>
						                 <!-- <a class="comment-button" href="#!"><i class="tf-ion-chatbubbles"></i>Reply</a>  -->
						            </div>

						            <p>{{$publicado->descripcion}}</p>
									<p class="pull-left" >
									@if($publicado->foto)
									<img class="media-object comment-avatar" src='{{URL::asset("/storage/$ruta->foto")}}' alt="" width="200" height="200">
						            @endif
								</p>
						        </div>

						    </li>
						    <!-- End Comment Item -->
							@endforeach
						   

						</ul>
				    </div>
					@auth
					@if(Session::has('mensaje'))
					<div class='alert {{Session::get('alert-class')}}'>{{Session::get('mensaje')}}</div>
					@endif
					<h3 class="post-sub-heading">Escribe tu comentario ...</h3>
				    <div class="post-comments-form">
					
						<form action="{{ route('comentariorutacabrio.store') }}" method="post" enctype="multipart/form-data">
				    		@include('plantillas.createcomentario')
				                <!-- Send Button -->
				               <div class="form-group col-md-12"> 
								<button type="reset" class="btn btn-small btn-main " name="Limpiar Comentario">
				                       Limpiar
				                    </button>
				                    <button type="submit" class="btn btn-small btn-main " name="Enviar Comentario">
				                        Enviar
				                    </button>
				            	</div> 
				            </div>
				        </form>
				    </div>
					@endauth
					@guest 
					<h3 class="post-sub-heading">Para escribir comentarios ...       <a href={{route('registrarse')}}>registrate</a></h3>
					@endguest
				</div>

			</div>
		</div>
	</div>
</section>
<footer class="footer section text-center">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<ul class="social-media">
					
					<li>
						<a href="https://www.instagram.com/rutacabrio/">
							<i class="tf-ion-social-instagram"></i>
						</a>
					</li>
					
					
				</ul>
				<ul class="footer-menu text-uppercase">
					<li>
						<a href="{{route('contacto')}}">CONTACTO</a>
					</li>
					
				</ul>
				<p class="copyright-text">Copyright &copy;2021, Designed &amp; Developed by <a href="">cousiñas</a></p>
			</div>
		</div>
	</div>
</footer>


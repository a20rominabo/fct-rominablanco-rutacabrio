<div class="page-wrapper">
	<div class="container">
		<div class="row">
		@foreach($postrutacabrios as $ruta)      

      		<div class="col-md-6">
		        <div class="post" >
		        	<div class="post-thumb paginar" >
		            @if($ruta->foto != null)
					
						<img class="img-responsive paginar fotitoruta" src='{{URL::asset("/storage/$ruta->foto")}}' width="455" height="455">
						
						@elseif($ruta->video != null)
					<div class="embed-responsive embed-responsive-16by9">
  						<iframe class="embed-responsive-item" src='{{URL::asset("/storage/$ruta->video")}}' sandbox width="455" height="455"></iframe>
					</div>
					@endif
		            </div>
		            <h2 class="post-title"><a href="">{{$ruta->titulo}}</a></h2>
		            <div class="post-meta">
		            	<ul>
		              		<li>
		                		<i class="tf-ion-ios-calendar"></i> {{$ruta->fecha}}
		              		</li>
		              		<li>
		                		<i class="tf-ion-android-person"></i> Editor Ruta Cabrio
		              		</li>
		              		<li>
		               			<i class="tf-ion-ios-pricetags"></i> {{$ruta->comunidadAutonoma}}, {{$ruta->ciudad}}
		              		</li>
		              		<li>
		                		<i class="tf-ion-chatbubbles"></i> comentarios
		              		</li>
		            	</ul>
		          	</div>
		          	<div class="post-content">
		           		<a href="{{route('contenido',$ruta)}}" class="btn btn-main">Seguir leyendo</a>
						<a href="{{$ruta->enlaceInstagram}}" class="btn btn-main">Instagram</a>
					</div>
				</div>
        	</div>
		@endforeach
 		</div>
	</div>
</div>	     		
<div class="text-center">
			{!! $postrutacabrios->links()!!}
			{{-- <ul class="pagination post-pagination">	
				
			</ul> --}}
</div>
<footer class="footer section text-center">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<ul class="social-media">
					
					<li>
						<a href="https://www.instagram.com/rutacabrio/">
							<i class="tf-ion-social-instagram"></i>
						</a>
					</li>
					
					
				</ul>
				<ul class="footer-menu text-uppercase">
					<li>
						<a href="{{route('contacto')}}">CONTACTO</a>
					</li>
					
				</ul>
				<p class="copyright-text">Copyright &copy;2021, Designed &amp; Developed by <a href="">cousiñas</a></p>
			</div>
		</div>
	</div>
</footer>


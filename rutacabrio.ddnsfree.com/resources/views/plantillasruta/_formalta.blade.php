@csrf

                    <div class="form-group">
                     <label for="titulo" class="form-label">Título del Post:</label> 
                      {{--{{ old('titulo',$postrutacabrio->titulo) }} {{ old('tipoRuta',$item->tipoRuta) }}  {{ old('foto',$item->foto) }}  {{ old('video',$item->video) }}  {{ old('enlaceInstagram',$item->enlaceInstagram) }} {{ old('enlaceYoutube',$item->enlaceYoutube) }}  --}}
                    <input type="text" class="form-control" id="titulo" name="titulo" value="" required maxlength="150">
                    @error('titulo')
                    <small class='alert alert-danger'>{{ $message }}</small>
                    @enderror
                    </div>
                     <div class="form-group">
                        <label for="tipoRuta" class="form-label">Tipo de Ruta:</label>
                        <input type="text" class="form-control" id="tipoRuta" name="tipoRuta" value="" maxlength="15">
                        @error('tipoRuta')
                        <small class='alert alert-danger'>{{ $message }}</small>
                        @enderror
                     </div>
                     <div class="form-group">
                        <label for="foto" class="form-label">Foto:</label>
                        <input type="file" class="form-control" id="foto" name="foto" value="" maxlength="100">
                        @if ($errors->has('foto'))
                        <small class='alert alert-danger'>{{ $errors->first('foto') }}</small>
                        @endif
                        @error('foto')
                                <small class='alert alert-danger'>{{ $message }}</small>
                            @enderror
                     </div>                     
                        <div class="form-group">
                           <label for="video" class="form-label">Video:</label>
                            <input type="file" class="form-control" id="video" name="video" value="" maxlength="100">
                            @if ($errors->has('video'))
                            <small class='alert alert-danger'>{{ $errors->first('video') }}</small>
                            @endif
                            @error('video')
                                <small class='alert alert-danger'>{{ $message }}</small>
                            @enderror
                        </div>
                        <div class="form-group" >
                     <label for="enlacesExterno" class="form-label">enlace Externo:</label>
    <input type="text" class="form-control" id="enlacesExterno" name="enlacesExterno" value="" maxlength="250" >
    @error('enlacesExterno')
    <small class='alert alert-danger'>{{ $message }}</small>
    @enderror
                     </div>
                       <div class="checkout-country-code clearfix">
                     <div class="form-group" >
                     <label for="coordenadaLatitud" class="form-label">Latitud:</label>
                    <input type="text" class="form-control" id="coordenadaLatitud" name="coordenadaLatitud" value="" maxlength="20">
                    @error('coordenadaLatitud')
                        <small class='alert alert-danger'>{{ $message }}</small>
                    @enderror
                     </div>
                      {{-- {{ old('coordenadaLatitud',$item->coordenadaLatitud) }} {{ old('coordenadaLongitud',$item->coordenadaLongitud) }} {{ old('descripcion',$item->descripcion) }} {{ old('fecha',$item->fecha) }} {{ old('comunidadAutonoma',$item->comunidadAutonoma) }}--}}
                     <div class="form-group" >
                     <label for="coordenadaLongitud" class="form-label">Longitud:</label>
                <input type="text" class="form-control" id="coordenadaLongitud" name="coordenadaLongitud" value="" maxlength="20">
                    @error('coordenadaLongitud')
                <small class='alert alert-danger'>{{ $message }}</small>
                    @enderror
                     </div>
                       </div>

                     <div class="form-group" >
                     <label for="descripcion" class="form-label">Descripcion:</label>
                    <input type="text" class="form-control" id="descripcion" name="descripcion" value="" maxlength="1000" >
                    @error('descripcion')
                    <small class='alert alert-danger'>{{ $message }}</small>
                    @enderror
                     </div>
                     <div class="form-group" >
                     <label for="fecha" class="form-label">Fecha :</label>
                        <input type="date" class="form-control" id="fecha" name="fecha" value="">
                    @error('fecha')
                    <small class='alert alert-danger'>{{ $message }}</small>
                    @enderror
                     </div>
                     <div class="checkout-country-code clearfix">
                     <div class="form-group" >
                     <label for="comunidadAutonoma" class="form-label">comunidad Autonoma:</label>
    <input type="text" class="form-control" id="comunidadAutonoma" name="comunidadAutonoma" value="" maxlength="250" >
    @error('comunidadAutonoma')
    <small class='alert alert-danger'>{{ $message }}</small>
    @enderror
    {{-- {{ old('ciudad',$item->ciudad) }}  {{ old('enlacesExternos',$item->enlacesExternos) }} --}}
                     </div>
                     <div class="form-group" >
                     <label for="ciudad" class="form-label">ciudad:</label>
    <input type="text" class="form-control" id="ciudad" name="ciudad" value="" maxlength="250" >
    @error('ciudad')
    <small class='alert alert-danger'>{{ $message }}</small>
    @enderror
                     </div>
                     </div>
                     
                       
                    
                  
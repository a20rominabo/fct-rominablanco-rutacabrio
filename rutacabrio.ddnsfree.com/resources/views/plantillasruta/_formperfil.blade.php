@csrf


                    <div class="form-group">
                    <label for="nombre" class="form-label fw-bold">Nombre : </label>
                    <input type="text" class="form-control" id="nombre" name="nombre" value="{{ old('nombre',$usuario->nombre) }}" required maxlength="150">
                    @error('nombre')
                    <small class='alert alert-danger'>{{ $message }}</small>
                    @enderror
                    </div>
                     <div class="form-group">
                        <label for="apellidos" class="form-label">Apellidos : </label>
                        <input type="text" class="form-control" id="apellidos" name="apellidos" value="{{ old('apellidos',$usuario->apellidos) }}" maxlength="150">
                        @error('apellidos')
                        <small class='alert alert-danger'>{{ $message }}</small>
                        @enderror
                     </div>
                     <div class="form-group">
                        <label for="login" class="form-label">Login :</label>
                        <input type="text" class="form-control" id="login" name="login" value="{{ old('login',$usuario->login) }}" maxlength="150">
                        @if ($errors->has('login'))
                        <small class='alert alert-danger'>{{ $errors->first('login') }}</small>
                        @endif
                     </div>         
                    <div class="form-group" >
                           <label for="telefono" class="form-label">Telefono :</label>
                            <input type="text" class="form-control" id="telefono" name="telefono" value="{{ old('telefono',$usuario->telefono) }}" maxlength="15" >
                            @error('telefono')
                                <small class='alert alert-danger'>{{ $message }}</small>
                            @enderror
                        </div>
                     <div class="form-group" >
                     <label for="tipoCoche" class="form-label">Tipo Coche:</label>
                    <input type="text" class="form-control" id="tipoCoche" name="tipoCoche" value="{{ old('tipoCoche',$usuario->tipoCoche) }}" maxlength="150" >
                     @error('tipoCoche')
                            <small class='alert alert-danger'>{{ $message }}</small>
                        @enderror
                     </div>
                       <div class="checkout-country-code clearfix">
                     <div class="form-group" >
                     <label for="email" class="form-label">Email:</label>
                    <input type="email" class="form-control" id="email" name="email" value="{{ old('email',$usuario->email) }}" maxlength="150">
                    @error('email')
                        <small class='alert alert-danger'>{{ $message }}</small>
                    @enderror
                     </div>
                      
                     <div class="form-group" >
                     <label for="password" class="form-label">Contraseña:</label>
                <input type="password" class="form-control" id="password" name="password" value="{{ old('password',$usuario->password) }}" maxlength="20">
                    @error('password')
                <small class='alert alert-danger'>{{ $message }}</small>
                    @enderror
                     </div>
                       </div>
          
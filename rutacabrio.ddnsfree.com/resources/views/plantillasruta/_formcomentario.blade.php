@csrf

   <div class="form-group">
      <label for="descripcion" class="form-label">Descripcion :</label>
      <input type="text" class="form-control" id="descripcion" name="descripcion" value="{{ old('descripcion',$item->descripcion) }}" required maxlength="150">
         @error('descripcion')
             <small class='alert alert-danger'>{{ $message }}</small>
         @enderror
      </div>
      <div class="form-group" >
         <label for="fecha" class="form-label">Fecha :</label>
         <input type="date" class="form-control" id="fecha" name="fecha" value="{{ old('fecha',$item->fecha) }}">
            @error('fecha')
               <small class='alert alert-danger'>{{ $message }}</small>
            @enderror
      </div>
      <div class="form-group">
            <label for="foto" class="form-label">Foto:</label>
            <input type="file" class="form-control" id="foto" name="foto" value="{{ old('foto',$item->foto) }}" maxlength="100">
               @if ($errors->has('foto'))
                  <small class='alert alert-danger'>{{ $errors->first('foto') }}</small>
               @endif
      </div>                     
                     
                    
                  
@extends('plantillaInicioFormulariosEditar')
@section('sectionHeader')
<h1>Ruta cabrio - Perfil {{auth()->user()->nombre}}</h1>			
@stop
@section('sectionPageHeader')					
<h1 class="page-name text-align-center">Bienvenid@ {{auth()->user()->nombre}}</h1>
	@if(Session::has('mensaje'))
	<div class="alert {{ Session::get('alert-class') }}">
    	{{ Session::get('mensaje') }}
	</div>
	@endif	
@stop
@section('central')

<h4 class="widget-title">Comentario</h4>
@foreach($publicacion as $item)

<form class="checkout-form" action="{{route('comentarioperfilcomunidad.update',$item->idcomentariocomunidad)}}" enctype="multipart/form-data" method="post"> 
 @method('PUT')  

@include('plantillasruta._formcomentario')
@endforeach
<input  class="btn btn-main mt-20" type="reset" value="limpiar"/>
<input  class="btn btn-main mt-20" type="submit" value="Actualizar"/>
<a href="{{route('perfilcomentario')}}" class="btn btn-main mt-20">Volver</a >
@stop

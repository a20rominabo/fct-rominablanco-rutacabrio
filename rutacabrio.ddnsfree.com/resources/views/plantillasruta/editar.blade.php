@extends('plantillaInicioFormulariosEditar')
@section('sectionHeader')
<h1>Ruta cabrio - Perfil {{auth()->user()->nombre}}</h1>			
@stop
@section('sectionPageHeader')					
<h1 class="page-name text-align-center">Bienvenid@ {{auth()->user()->nombre}}</h1>
	@if(Session::has('mensaje'))
	<div class="alert {{ Session::get('alert-class') }}">
    	{{ Session::get('mensaje') }}
	</div>
	@endif	
@stop

@section('central')
<div class="page-wrapper">
   <div class="checkout shopping">
      <div class="container">
         <div class="row">
            <div class="col-md-8">
               <div class="block billing-details">
			   @foreach ($publicacion as $item ) 
                  <h4 class="widget-title">Posts</h4>
                  <form class="checkout-form" action="{{route('rutacabrio.update',$item->idpostcomunidad)}}" method="post" enctype="multipart/form-data"> 
				  @method('PUT')
	
				@include('plantillasruta._form') 
				@endforeach 
				<input  class="btn btn-main mt-20" type="reset" value="limpiar"/>
				<input  class="btn btn-main mt-20" type="submit" value="Actualizar"/>
				<a class="btn btn-main mt-20" href="{{route('perfilpost')}}">Volver</a>
					</form>
               </div>
			</div>
		</div>
	</div>
</div>
@stop

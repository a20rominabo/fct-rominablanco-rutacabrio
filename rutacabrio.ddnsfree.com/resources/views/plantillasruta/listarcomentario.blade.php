@extends('plantillaInicioSesiones')
@section('sectionHeader')
<h1>Ruta cabrio - Perfil {{auth()->user()->nombre}}</h1>			
@stop
@section('sectionPageHeader')					
<h1 class="page-name text-align-center">Bienvenid@ {{auth()->user()->nombre}}</h1>
	@if(Session::has('mensaje'))
	<div class="alert {{ Session::get('alert-class') }}">
    	{{ Session::get('mensaje') }}
	</div>
	@endif	
@stop
@section('usuarioHeader')
	<div class="container-fluid">
    	<div class="row">
      		<div class="col-md-12">
        		<ul class="list-inline dashboard-menu text-center">          
          			<li><a  href="{{route('perfilruta')}}">Posts RutaCabrio</a></li>
		  			<li><a  href="{{route('listarusuarios')}}">Editar usuarios</a></li>
          			<li><a class="active"href="{{route ('perfilcomentario')}}">Mis comentarios</a></li>
          			<li><a href="{{route('perfilpostcomunidad')}}">Ver Post Comunidad</a></li>
          			<li><a   href="{{route('perfil')}}">Perfil</a></li>
        		</ul>
			</div>
		</div>
	</div>
@stop
@section('usuarioHeader2')
<div class="container-fluid">
    <div class="row">
      	<div class="col-md-12">
        	<ul class="list-inline dashboard-menu text-center">
          		<li><a href="{{route('perfilpost')}}">Posts</a></li>
          		<li><a class="active" href="{{route('perfilcomentario')}}">Mis comentarios</a></li>
          		<li><a   href="{{route('perfil')}}">Perfil</a></li>
        	</ul>
		</div>
	</div>
</div>
@stop
@section('central')

<div class="page-wrapper">
  	<div class="purchase-confirmation shopping">
    	<div class="container">
      		<div class="row">
        		<div class="container-fluid col-12">
          			<div class="block ">
            			<div class="purchase-confirmation-details table-responsive">
				            <table id="purchase-receipt" class="table ">
				                <thead>
									<tr>
					            		<th class="text-center"><strong>Comentario en RutaCabrio</strong></th>                               
                               			<th class="text-center"><strong>Editar</strong></th>
                               			<th class="text-center"><strong>Borrar</strong></th>
                               			<th class="text-center"><strong>Info</strong></th>
					                </tr>
				                </thead>
								<tbody>
									@foreach($postpublicados as $publicacion) 
				                  	<tr>
                           				<td class="">{{\Carbon\Carbon::parse($publicacion->fecha)->format('d/m/Y')}}</td>
				                    	<td  class="text-center">
                              				<div class="container-fluid d-flex justify-content-center align-items-center">
  												<a href="{{route('comentarioperfil.edit',$publicacion)}}" class="btn btn-main" >Editar</a><br /><br />
                                     		</div>
                              			</td>
                              			<td  class="text-center">
                              				<div class="container-fluid d-flex justify-content-between align-items-center">
												<form action="{{route('comentarioperfil.destroy',$publicacion)}} " method="post" enctype="multipart/form-data">
									  				@csrf 
									  				@method('DELETE') 
									  				<input type="submit" class="btn btn-main " value="Borrar" /></form></a><br /><br />
                                			</div>
                              			</td>
                              			<td  class="text-center">
                              				<div class="container-fluid d-flex justify-content-between align-items-center">
                         						<a href="{{route('comentarioperfil.show',$publicacion)}}" class="btn btn-main text-center" >Info</a>
                                   			</div>
                              			</td>
				                  	</tr>
									@endforeach
								</tbody>
				            </table>
              			</div>
            		</div>
          		</div>
        	</div>
      	</div>
    </div>
</div>
<div class="page-wrapper">
  	<div class="purchase-confirmation shopping">
    	<div class="container">
      		<div class="row">
        		<div class="container-fluid col-md-12">
          			<div class="block ">
            			<div class="purchase-confirmation-details table-responsive">
				            <table id="purchase-receipt" class="table ">
				                <thead>
									<tr>
					            		<th class="text-center"><strong>Comentario en Comunidad</strong></th>                               
                               			<th class="text-center"><strong>Editar</strong></th>
                               			<th class="text-center"><strong>Borrar</strong></th>
                               			<th class="text-center"><strong>Info</strong></th>
					                </tr>
				                </thead>
								<tbody>
                              		@foreach($postcomunidades as $publicacioncom) 
				                  	<tr>
                           				<td class="">{{\Carbon\Carbon::parse($publicacioncom->fecha)->format('d/m/Y')}}</td>
				                    	<td  class="text-center">
                              				<div class="container-fluid d-flex justify-content-center align-items-center">
   												<a href="{{route('comentarioperfilcomunidad.edit',$publicacioncom)}}" class="btn btn-main" >Editar</a><br /><br />
                                     		</div>
                              			</td>
                              			<td  class="text-center">
                              				<div class="container-fluid d-flex justify-content-between align-items-center">
								  	            <form action="{{route('comentarioperfilcomunidad.destroy',$publicacioncom)}} " method="post">
									  			@csrf 
									  			@method('DELETE') 
									  			<input type="submit" class="btn btn-main " value="Borrar" /></form></a><br /><br />
                                			</div>
                              			</td>
                              			<td  class="text-center">
                              				<div class="container-fluid d-flex justify-content-between align-items-center">
                             					<a href="{{route('comentarioperfilcomunidad.show',$publicacioncom)}}" class="btn btn-main text-center" >Info</a>
                                   			</div>
                              			</td>
				                  	</tr>
                            		@endforeach
								</tbody>
				            </table>
              			</div>
            		</div>
          		</div>
        	</div>
      	</div>
    </div>
</div>
<div class="container-fluid ">
<a href="{{route('perfil')}}" class="btn btn-main mt-20">Volver</a >
</div>
@stop

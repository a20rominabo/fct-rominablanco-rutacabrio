@extends('plantillaInicioFormulariosEditar')
@section('sectionHeader')
<h1>Ruta cabrio - Perfil {{auth()->user()->nombre}}</h1>			
@stop
@section('sectionPageHeader')					
<h1 class="page-name text-align-center">Bienvenid@ {{auth()->user()->nombre}}</h1>
	@if(Session::has('mensaje'))
	<div class="alert {{ Session::get('alert-class') }}">
    	{{ Session::get('mensaje') }}
	</div>
	@endif	
@stop

@section('central')
<div class="page-wrapper">
  	<div class="purchase-confirmation shopping">
    	<div class="container">
      		<div class="row">
        		<div class="col-12">
          			<div class="block ">
            			<div class="purchase-confirmation-details table-responsive">
				            <table id="purchase-receipt" class="table">
				                <thead>
									<tr>
					                    <th><strong>Comentario</strong></th>
					                </tr>
				                </thead>

				                <tbody>
								@foreach($postpublicado as $item)
									
									<tr>
				                      	<td><strong>Fecha:</strong></td>
				                      	<td>{{$item->fecha}}</td>
				                    </tr>	
				                  	<tr>
				                    	<td class=""><strong>Descripcion :</strong></td>
				                    	<td class="">{{$item->descripcion}}</td>
				                  	</tr>

				                  	<tr>
				                    	<td><strong>Foto :</strong></td>
										<td>
										@if($item->foto)	
										<img class="media-object comment-avatar" src='{{URL::asset("/storage/$item->foto")}}' alt="" width="200" height="200"></td>
				                    	@endif
				                  	</tr>
				                  	
                            
								@endforeach
				                </tbody>
				            </table>
              			</div>
            		</div>
          		</div>
        	</div>
      	</div>
    </div>
</div>
<div class="container-fluid ">
<a href="{{route('perfilcomentario')}}" class="btn btn-main mt-20">Volver</a >
</div>

@stop			

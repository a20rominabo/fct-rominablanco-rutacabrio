@extends('plantillaInicioFormulariosEditar')
@section('sectionHeader')
<h1>Ruta cabrio - Perfil {{auth()->user()->nombre}}</h1>			
@stop
@section('sectionPageHeader')					
<h1 class="page-name text-align-center">Bienvenid@ {{auth()->user()->nombre}}</h1>
	@if(Session::has('mensaje'))
	<div class="alert {{ Session::get('alert-class') }}">
    	{{ Session::get('mensaje') }}
	</div>
	@endif	
@stop
@section('usuarioHeader')
	<div class="container-fluid">
    	<div class="row">
      		<div class="col-md-12">
        		<ul class="list-inline dashboard-menu text-center">          
          			<li><a  href="{{route('perfilruta')}}">Posts RutaCabrio</a></li>
		  			<li><a class="active" href="{{route('listarusuarios')}}">Editar usuarios</a></li>
          			<li><a href="{{route ('perfilcomentario')}}">Mis comentarios</a></li>
          			<li><a href="{{route('perfilpostcomunidad')}}">Ver Post Comunidad</a></li>
          			<li><a   href="{{route('perfil')}}">Perfil</a></li>
        		</ul>
@stop
@section('central')
<div class="page-wrapper">
   <div class="checkout shopping">
      <div class="container">
         <div class="row">
            <div class="col-md-8">
               <div class="block billing-details">
			   		@foreach ($publicacion as $item ) 
                	<h4 class="widget-title">Usuario :  {{$item->nombre}}</h4>
                	<form class="checkout-form" action="{{route('actualizarusuarios',$item->id)}}" method="post">  
					@method('PUT') 
	
 					@include('plantillasruta._formusuario')  
					@endforeach 
					<input  class="btn btn-main mt-20" type="reset" value="limpiar"/>
					<input  class="btn btn-main mt-20" type="submit" value="Actualizar"/>
					<a href="{{route('listarusuarios')}}" class="btn btn-main mt-20">Volver</a >

				</form>
               </div>
			</div>
		</div>
	</div>
</div>
@stop

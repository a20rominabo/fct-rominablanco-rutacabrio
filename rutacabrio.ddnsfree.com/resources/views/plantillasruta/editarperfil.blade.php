@extends('plantillaInicioFormulariosEditar')
@section('sectionHeader')
<h1>Ruta cabrio</h1>			
@stop
@section('sectionMenu')
<!-- Pages -->
	<li class="dropdown full-width dropdown-slide">
		<a href="#!" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="350"
							role="button" aria-haspopup="true" aria-expanded="false"> Contenido <span
								class="tf-ion-ios-arrow-down"></span></a>
		<div class="dropdown-menu">
			<div class="row">
				<!-- Introduction -->
				<div class="col-sm-3 col-xs-12">
					<ul>
						<li class="dropdown-header">Publicaciones</li>
						<li role="separator" class="divider"></li>
						<li><a href="{{route('perfilpost')}}">Post</a></li>
						<li><a href="{{route('perfilcomentario')}}">Comentarios</a></a></li>									
					</ul>
				</div>
				<!-- Contact -->
				<div class="col-sm-3 col-xs-12">
					<ul>
						<li  class="dropdown-header">Perfil</li>
						<li role="separator" class="divider"></li>
						<li><a href="{{route('perfil')}}">Ver Perfil</a></li>
					</ul>
				</div>
			</div><!-- / .row -->
		</div><!-- / .dropdown-menu -->
	</li><!-- / Pages -->
@stop
@section('sectionPageHeader')					
<h1 class="page-name text-align-center">Bienvenid@ {{auth()->user()->nombre}}</h1>
<p>Modificación del perfil</p>
	@if(Session::has('mensaje'))
	<div class="alert {{ Session::get('alert-class') }}">
    	{{ Session::get('mensaje') }}
	</div>
	@endif	
@stop				
@section('central')
<div class="page-wrapper">
   	<div class="checkout shopping">
      	<div class="container">
         	<div class="row">
            	<div class="col-md-8">
               		<div class="block billing-details">
			  			<h4 class="widget-title">Datos del perfil</h4>
							<form class="checkout-form" action="{{route('modificar',$id)}}" method="post"> 
				  				@method('PUT')
								@include('plantillasruta._formperfil') 
								<input  class="btn btn-main mt-20" type="reset" value="limpiar"/>
								<input  class="btn btn-main mt-20" type="submit" value="Actualizar"/>
								<a href="{{route('perfil')}}" class="btn btn-main mt-20">Volver</a >
							</form>
               		</div>
				</div>
			</div>
		</div>
	</div>
</div>
@stop



	
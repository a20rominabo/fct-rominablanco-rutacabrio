@extends('plantillaInicioFormulariosEditar')
@section('sectionHeader')
<h1>Ruta cabrio - Perfil {{auth()->user()->nombre}}</h1>			
@stop
@section('sectionPageHeader')					
<h1 class="page-name text-align-center">Bienvenid@ {{auth()->user()->nombre}}</h1>
	@if(Session::has('mensaje'))
	<div class="alert {{ Session::get('alert-class') }}">
    	{{ Session::get('mensaje') }}
	</div>
	@endif	
@stop

@section('central')


<div class="page-wrapper">
  	<div class="purchase-confirmation shopping">
    	<div class="container">
      		<div class="row">
        		<div class="col-md-12">
          			<div class="block ">
            			<div class="purchase-confirmation-details table-responsive">
				            <table id="purchase-receipt" class="table-responsive">
				                <thead>
									<tr>
					                    <th><strong>Post</strong></th>
					                </tr>
				                </thead>

				                <tbody>
									@foreach($postpublicado as $item)
										
										
										<tr>
											<td class=""><strong>Titulo :</strong></td>
											<td class="">{{$item->titulo}}</td>
										</tr>


										<tr>
											<td><strong>Tipo de ruta :</strong></td>
											<td>{{$item->tipoRuta}}</td>
										</tr>
										<tr>
											<td><strong>Foto :</strong></td>
											<td><img class="media-object comment-avatar" src='{{URL::asset("/storage/$item->foto")}}' alt="" width="200" height="200"></td>
											<td></td>
										</tr>
										<tr>
											<td><strong>Video :</strong></td>
											<td>{{$item->video}}</td>
										</tr>			                    
                             
										<tr>
													<td><strong>Coordenada Latitud:</strong></td>
													<td>{{$item->coordenadaLatitud}}</td>
										</tr>
										<tr>
													<td><strong>Coordenada Longitud:</strong></td>
													<td>{{$item->coordenadaLongitud}}</td>
												</tr>
										<tr>
													<td><strong>Descripcion:</strong></td>
													<td>{{$item->descripcion}}</td>
												</tr>
										<tr>
													<td><strong>Fecha:</strong></td>
													<td>{{$item->fecha}}</td>
												</tr>
										<tr>
													<td><strong>Comunidad Autonoma:</strong></td>
													<td>{{$item->comunidadAutonoma}}</td>
												</tr>
										<tr>
													<td><strong>Ciudad:</strong></td>
													<td>{{$item->ciudad}}</td>
												</tr>
										<tr>
													<td><strong>Enlaces Externos:</strong></td>
													<td>{{$item->enlacesExterno}}</td>
												</tr>
                            
									@endforeach
				                </tbody>
				            </table>
              			</div>
            		</div>
          		</div>
        	</div>
      	</div>
    </div>
</div>
<div class="container-fluid ">
	@if((auth()->user()->categoria)=== 'usuariorutacabrio')
	<a href="{{route('perfilpostcomunidad')}}" class="btn btn-main mt-20">Volver</a >
	@else
	<a href="{{route('perfilpost')}}" class="btn btn-main mt-20">Volver</a >
	@endif

</div>
@stop
	
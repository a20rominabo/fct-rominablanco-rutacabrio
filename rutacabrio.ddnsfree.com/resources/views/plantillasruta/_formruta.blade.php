@csrf

                    <div class="form-group">
                    <label for="titulo" class="form-label">Título del Post:</label>
                    <input type="text" class="form-control" id="titulo" name="titulo" value="{{ old('titulo',$item->titulo) }}" required maxlength="150">
                    @error('titulo')
                    <small class='alert alert-danger'>{{ $message }}</small>
                    @enderror
                    </div>
                     <div class="form-group">
                        <label for="tipoRuta" class="form-label">Tipo de Ruta:</label>
                        <input type="text" class="form-control" id="tipoRuta" name="tipoRuta" value="{{ old('tipoRuta',$item->tipoRuta) }}" maxlength="15">
                        @error('tipoRuta')
                        <small class='alert alert-danger'>{{ $message }}</small>
                        @enderror
                     </div>
                     <div class="form-group">
                        <label for="foto" class="form-label">Foto:</label>
                        <input type="file" class="form-control" id="foto" name="foto" value="{{ old('foto',$item->foto) }}" maxlength="500">
                        @if ($errors->has('foto'))
                        <small class='alert alert-danger'>{{ $errors->first('foto') }}</small>
                        @endif
                     </div>                     
                        <div class="form-group">
                           <label for="video" class="form-label">Video:</label>
                            <input type="file" class="form-control" id="video" name="video" value="{{ old('video',$item->video) }}" maxlength="500">
                            @if ($errors->has('video'))
                            <small class='alert alert-danger'>{{ $errors->first('video') }}</small>
                            @endif
                        </div>
                        <div class="form-group" >
                           <label for="enlaceInstagram" class="form-label">enlace Instagram:</label>
                            <input type="text" class="form-control" id="enlaceInstagram" name="enlaceInstagram" value="{{ old('enlaceInstagram',$item->enlaceInstagram) }}" maxlength="200" >
                            @error('enlaceInstagram')
                                <small class='alert alert-danger'>{{ $message }}</small>
                            @enderror
                        </div>
                     <div class="form-group" >
                     <label for="enlaceYoutube" class="form-label">enlace Youtube:</label>
                    <input type="text" class="form-control" id="enlaceYoutube" name="enlaceYoutube" value="{{ old('enlaceYoutube',$item->enlaceYoutube) }}" maxlength="200" >
                     @error('enlaceYoutube')
                            <small class='alert alert-danger'>{{ $message }}</small>
                        @enderror
                     </div>
                       <div class="checkout-country-code clearfix">
                     <div class="form-group" >
                     <label for="coordenadaLatitud" class="form-label">Latitud:</label>
                    <input type="text" class="form-control" id="coordenadaLatitud" name="coordenadaLatitud" value="{{ old('coordenadaLatitud',$item->coordenadaLatitud) }}" maxlength="20">
                    @error('coordenadaLatitud')
                        <small class='alert alert-danger'>{{ $message }}</small>
                    @enderror
                     </div>
                      
                     <div class="form-group" >
                     <label for="coordenadaLongitud" class="form-label">Longitud:</label>
                <input type="text" class="form-control" id="coordenadaLongitud" name="coordenadaLongitud" value="{{ old('coordenadaLongitud',$item->coordenadaLongitud) }}" maxlength="20">
                    @error('coordenadaLongitud')
                <small class='alert alert-danger'>{{ $message }}</small>
                    @enderror
                     </div>
                       </div>

                     <div class="form-group" >
                     <label for="descripcion" class="form-label">Descripcion:</label>
                    <input type="text" class="form-control" id="descripcion" name="descripcion" value="{{ old('descripcion',$item->descripcion) }}" maxlength="1000" >
                    @error('descripcion')
                    <small class='alert alert-danger'>{{ $message }}</small>
                    @enderror
                     </div>
                     <div class="form-group" >
                     <label for="fecha" class="form-label">Fecha :</label>
                        <input type="date" class="form-control" id="fecha" name="fecha" value="{{ old('fecha',$item->fecha) }}">
                    @error('fecha')
                    <small class='alert alert-danger'>{{ $message }}</small>
                    @enderror
                     </div>
                     <div class="checkout-country-code clearfix">
                     <div class="form-group" >
                     <label for="comunidadAutonoma" class="form-label">comunidad Autonoma:</label>
    <input type="text" class="form-control" id="comunidadAutonoma" name="comunidadAutonoma" value="{{ old('comunidadAutonoma',$item->comunidadAutonoma) }}" maxlength="250" >
    @error('comunidadAutonoma')
    <small class='alert alert-danger'>{{ $message }}</small>
    @enderror
                     </div>
                     <div class="form-group" >
                     <label for="ciudad" class="form-label">ciudad:</label>
    <input type="text" class="form-control" id="ciudad" name="ciudad" value="{{ old('ciudad',$item->ciudad) }}" maxlength="250" >
    @error('ciudad')
    <small class='alert alert-danger'>{{ $message }}</small>
    @enderror
                     </div>
                     </div>
                     <div class="form-group" >
                     <label for="enlacesExternos" class="form-label">enlaces Externos:</label>
    <input type="text" class="form-control" id="enlacesExternos" name="enlacesExternos" value="{{ old('enlacesExternos',$item->enlacesExternos) }}" maxlength="250" >
    @error('enlacesExternos')
    <small class='alert alert-danger'>{{ $message }}</small>
    @enderror
                     </div>
                       
                    
                  
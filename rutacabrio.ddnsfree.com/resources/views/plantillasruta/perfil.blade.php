@extends('plantillaInicioSesiones')
@section('sectionHeader')
<h1>Ruta cabrio- Perfil {{auth()->user()->nombre}}</h1>			
@stop

@section('sectionPageHeader')					
<h1 class="page-name text-align-center">Bienvenid@ {{auth()->user()->nombre}}</h1>
	@if(Session::has('mensaje'))
	<div class="alert {{ Session::get('alert-class') }}">
    	{{ Session::get('mensaje') }}
	</div>
	@endif	
@stop
@section('usuarioHeader')
<div class="row ">
  <div class="col-md-12">
    <ul class="list-inline dashboard-menu text-center">          
      <li><a  href="{{route('perfilruta')}}">Posts RutaCabrio</a></li>
		  <li><a href="{{route('listarusuarios')}}">Editar usuarios</a></li>
      <li><a href="{{route ('perfilcomentario')}}">Mis comentarios</a></li>
      <li><a href="{{route('perfilpostcomunidad')}}">Ver Post Comunidad</a></li>
      <li><a  class="active" href="{{route('perfil')}}">Perfil</a></li>
    </ul>
  </div>
</div>
@stop 
@section('usuarioHeader2')
<div class="container-fluid">
  <div class="row">
    <div class="col-md-12">
      <ul class="list-inline dashboard-menu text-center">
        <li><a href="{{route('perfilpost')}}">Posts</a></li>
        <li><a href="{{route('perfilcomentario')}}">Mis comentarios</a></li>
        <li><a class="active"  href="{{route('perfil')}}">Perfil</a></li>
      </ul>
    </div>
  </div>
</div>
@stop
@section('central')
<div class="dashboard-wrapper dashboard-user-profile" style="margin-top:90px;">
  <div class="media">
    <div class="media-body ">
      <ul class="user-profile-list">
        <li><span>Nombre:</span>{{ auth()->user()->nombre }}</li>
        <li><span>Apellido:</span>{{ auth()->user()->apellidos }}</li>
        <li><span>Login:</span>{{ auth()->user()->login }}</li>
        <li><span>Categoria:</span>{{ auth()->user()->categoria }}</li>
				<li><span>telefono:</span>{{ auth()->user()->telefono }}</li>
        <li><span>tipoCoche:</span>{{ auth()->user()->tipoCoche }}</li>
				<li><span>email:</span>{{ auth()->user()->email }}</li>
				<li><a href=""></a></li>
      </ul>
      <div class="container-fluid ">    
        <a href="{{route('editarperfil',$id)}}" class="btn btn-main mt-20">Editar perfil</a >
      </div>
    </div>
  </div>
</div>
@stop
	
<!DOCTYPE html>
<html lang="en">

<head>

	<!-- Basic Page Needs
  ================================================== -->
	<meta charset="utf-8">
	<title>Rutacabrio - POST </title>

	<!-- Mobile Specific Metas
  ================================================== -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="description" content="Construction Html5 Template">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=5.0">
	<meta name="author" content="Romina Blanco Ortega">
	<meta name="generator" content="">

	<!-- Favicon -->
	<link rel="shortcut icon" type="image/x-icon" href="images/r.png" />

	<!-- Themefisher Icon font -->
	<link rel="stylesheet" href="plugins/themefisher-font/style.css">
	<!-- bootstrap.min css -->
	<link rel="stylesheet" href="plugins/bootstrap/css/bootstrap.min.css">

	<!-- Animate css -->
	<link rel="stylesheet" href="plugins/animate/animate.css">
	<!-- Slick Carousel -->
	<link rel="stylesheet" href="plugins/slick/slick.css">
	<link rel="stylesheet" href="plugins/slick/slick-theme.css">

	<!-- Main Stylesheet -->
	<link rel="stylesheet" href="css/style.css">

</head>

<body id="body">

	<!-- Start Top Header Bar -->
	<section class="top-header">
		<div class="container">
			<div class="row">

				<div class="col-md-12 col-xs-12 col-sm-12">
					<!-- Site Logo -->
					<div class="logo text-center">
						<h1>Ruta cabrio</h1>
					</div>
				</div>

			</div>
		</div>
	</section><!-- End Top Header Bar -->


	<!-- Main Menu Section -->
	<section class="menu">
		<nav class="navbar navigation">
			<div class="container">
				<div class="navbar-header">
					<h2 class="menu-title">Main Menu</h2>
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
						aria-expanded="false" aria-controls="navbar">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>

				</div><!-- / .navbar-header -->

				<!-- Navbar Links -->
				<div id="navbar" class="navbar-collapse collapse text-center">
					<ul class="nav navbar-nav">

						<!-- INICIO -->
						<li class="dropdown ">
							<a href="{{route('inicio')}}">Inicio</a>
						</li><!-- / INICIO -->
						<!-- RUTACABRIO -->
						<li class="dropdown ">
							<a href="{{route('postruta')}}">Rutacabrio</a>
						</li><!-- / RUTACABRIO -->
						<!-- COMUNIDAD  -->
						<li class="dropdown ">
							<a href="{{route('postcomunidad')}}">Comunidad</a>
						</li><!-- / COMUNIDAD -->
					</ul><!-- / .nav .navbar-nav -->
				</div>
				<!--/.navbar-collapse -->
			</div><!-- / .container -->
		</nav>
	</section>

	{{-- <section class="page-header">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="content">
						<h1 class="page-name"> {{$ruta->titulo}}</h1>
						<ol class="breadcrumb">
							{{-- <li><a href="{{route('postruta')}}">Rutacabrio</a></li>
							{{-- <li class="active">Post : {{$ruta->titulo}}</li> 
						</ol>
					</div>
				</div>
			</div>
		</div>
	</section> --}} 

	
	

	<footer class="footer section text-center">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<ul class="social-media">

						<li>
							<a href="https://www.instagram.com/rutacabrio/">
								<i class="tf-ion-social-instagram"></i>
							</a>
						</li>

					</ul>
					<ul class="footer-menu text-uppercase">
						<li>
							<a href="{{route('contacto')}}">CONTACTO</a>
						</li>

						<li>
							<a href="{{route('policy')}}">Politica Privacidad</a>
						</li>
					</ul>
					<p class="copyright-text">Copyright &copy;2022, Designed &amp; Developed by <a href="">Cousiñas</a>
					</p>
				</div>
			</div>
		</div>
	</footer>

	<!-- 
    Essential Scripts
    =====================================-->

	<!-- Main jQuery -->
	<script src="plugins/jquery/dist/jquery.min.js"></script>
	<!-- Bootstrap 3.1 -->
	<script src="plugins/bootstrap/js/bootstrap.min.js"></script>
	<!-- Bootstrap Touchpin -->
	<script src="plugins/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js"></script>
	<!-- Instagram Feed Js -->
	<script src="plugins/instafeed/instafeed.min.js"></script>
	<!-- Video Lightbox Plugin -->
	<script src="plugins/ekko-lightbox/dist/ekko-lightbox.min.js"></script>
	<!-- Count Down Js -->
	<script src="plugins/syo-timer/build/jquery.syotimer.min.js"></script>

	<!-- slick Carousel -->
	<script src="plugins/slick/slick.min.js"></script>
	<script src="plugins/slick/slick-animation.min.js"></script>

	<!-- Google Mapl
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCC72vZw-6tGqFyRhhg5CkF2fqfILn2Tsw"></script>
    <script type="text/javascript" src="plugins/google-map/gmap.js"></script>-->

	<!-- Main Js File -->
	{{-- <script src="js/script.js"></script> --> --}}



</body>

</html>
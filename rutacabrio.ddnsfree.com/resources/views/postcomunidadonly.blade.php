@extends('plantillaInicioRutas')
@section('sectionHeader')
<h1>Ruta cabrio Comunidad</h1>		
@stop
@section('sectionMenu')
<!-- rutacabrio -->
	<li class="dropdown ">
		<a href="{{route('postruta')}}">Rutacabrio</a>
	</li><!-- / rutacabrio -->	
<!-- COMUNIDAD  -->
	<li class="dropdown ">
		<a href="{{route('postcomunidad')}}">Comunidad</a>
	</li><!-- / COMUNIDAD -->
<!-- Pages -->
	<li class="dropdown full-width dropdown-slide">
		<a href="#!" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="350"
							role="button" aria-haspopup="true" aria-expanded="false"> Contenido <span
								class="tf-ion-ios-arrow-down"></span></a>
		<div class="dropdown-menu">
			<div class="row">
				<!-- Introduction -->
				<div class="col-sm-3 col-xs-12">
					<ul>
						<li class="dropdown-header">Años</li>
						<li role="separator" class="divider"></li>
						<li><a href="">2022</a></li>
						<li><a href="">2021</a></li>
						<li><a href="">2020</a></li>
					</ul>
				</div>
				<!-- Contact -->
				<div class="col-sm-3 col-xs-12">
					<ul>
						<li class="dropdown-header">Año 2022</li>
						<li role="separator" class="divider"></li>
						<li><a href="">Enero</a></li>
					</ul>
				</div>
			</div><!-- / .row -->
		</div><!-- / .dropdown-menu -->
	</li><!-- / Pages -->
@stop	
@section('sectionPageHeader')			
<h1 class="page-name"> {{$ruta->titulo}}</h1>
	<ol class="breadcrumb">
		<li><a href="{{route('postcomunidad')}}">Comunidad</a></li>
		<li class="active">Post : {{$ruta->titulo}}</li>
	</ol>
					
@foreach($usuarios as $usuario)
@stop
@section('contenido')
<section class="page-wrapper">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="post post-single">
					<div class="post-thumb">
						<img class="img-responsive fotito" src='{{URL::asset("/storage/$ruta->foto")}}' alt="">
					</div>
					<h2 class="post-title">{{$ruta->titulo}} </h2>
					<div class="post-meta">
						<ul>
							<li>
								<i class="tf-ion-ios-calendar"></i>{{ \Carbon\Carbon::parse($ruta->fecha)->format('d/m/Y') }}
							</li>
							<li>
								<i class="tf-ion-android-person"></i> Escrito por {{$usuario->login}}
							</li>
							<li>
								<i class="tf-ion-ios-pricetags"></i>{{$ruta->comunidadAutonoma}},{{$ruta->ciudad}}
							</li>
							<li>
								<p ><i class="tf-ion-chatbubbles"></i>{{$comentarios}}  comentarios</p>
							</li>
						</ul>
					</div>
					<div class="post-content post-excerpt">
						<p>{{$ruta->descripcion}}</p>
						<blockquote class="quote-post">
				            <a href="{{$ruta->enlacesExterno}}">{{$ruta->enlacesExterno}}</a>
				        </blockquote>
				        <img src='' width="320" /><br />
				        
				    </div>
					<div class="contact-details col-md-12 " >
						<div class="google-map">
							<div id="map" data-value="{{$ruta->titulo}}">
							<div id="latitud" data-value="{{$ruta->coordenadaLatitud}}"></div>
							<div id="longitud" data-value="{{$ruta->coordenadaLongitud}}"></div>
							</div>
						</div>
						<ul class="contact-short-info" >
							<li>
								<!-- tf-ion-android-globe -->
								<i class="tf-map2">  {{$ruta->coordenadaLatitud}} , {{$ruta->coordenadaLongitud}} </i>
								<span></span>
							</li>
						</ul>		
					</div>
				   
								
					@if(null !==(auth()->user()) && (auth()->user()->categoria)=== 'usuariorutacabrio')
					
					<div class="post-comments">
				    	<h3 class="post-sub-heading pt-2">Borrado del post :</h3>
						<!-- <form action="{{route('comunidadpost.destroy',$ruta)}} " enctype="multipart/form-data" method="post">@csrf @method('DELETE') <input type="submit" class="btn btn-main " value="Borrar" /></form></a><br /><br /> -->
						<h5>Información: Borrado del post no disponible en estos momentos</h5>
					</div>
					@endif
				    <div class="post-comments">
				    	<h3 class="post-sub-heading">{{$comentarios}} Comentarios</h3>
						<ul class="media-list comments-list m-bot-50 clearlist">
							@foreach($publicaciones as $publicado)				
						   <!-- Comment Item start-->
						    <li class="media">
						        <div class="media-body">
						            <div class="comment-info">
						                <div class="comment-author">
						                    <a href="#!"></a>
						                </div>
						                <time>{{\Carbon\Carbon::parse($publicado->fecha)->format('d/m/Y')}}</time>
						                {{-- <a class="comment-button" href="#!"><i class="tf-ion-chatbubbles"></i>Reply</a> --}}
						            </div>
						            <p>{{$publicado->descripcion}}</p>
									<a class="pull-left" href="#!">
									@if($publicado->foto)
						            <img class="media-object comment-avatar" src='{{URL::asset("/storage/$ruta->foto")}}' alt="" width="200" height="200">
						        	@endif
								</a>
						        </div>
						    </li>
						    <!-- End Comment Item -->
							@endforeach
							@endforeach
						</ul>
				    </div>
					@auth
					@if(Session::has('mensaje'))
					<div class='alert {{Session::get('alert-class')}}'>{{Session::get('mensaje')}}</div>
					@endif
					<h3 class="post-sub-heading">Escribe tu comentario ...</h3>
				    <div class="post-comments-form">					
						<form action="{{ route('comentariocomunidad.store') }}" method="post">
				    		 @include('plantillas.createcomentariocom') 
				                <!-- Send Button -->
				               <div class="form-group col-md-12"> 
								<button type="reset" class="btn btn-small btn-main " name="Limpiar Comentario">
				                       Limpiar
				                    </button>
				                    <button type="submit" class="btn btn-small btn-main " name="Enviar Comentario">
				                        Enviar
				                    </button>
				            	</div> 
				            </div>
				        </form>
				    </div>
					@endauth
				</div>
			</div>
		</div>
	</div>
</section>

<footer class="footer section text-center comunidad">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<ul class="social-media">
					
					<li>
						<a href="https://www.instagram.com/rutacabrio/">
							<i class="tf-ion-social-instagram"></i>
						</a>
					</li>
					
					
				</ul>
				<ul class="footer-menu text-uppercase">
					<li>
						<a href="{{route('contacto')}}">CONTACTO</a>
					</li>
					
				</ul>
				<p class="copyright-text">Copyright &copy;2021, Designed &amp; Developed by <a href="">cousiñas</a></p>
			</div>
		</div>
	</div>
</footer>

@stop
<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Mail;
use App\Models\User;
use App\Mail\Enviaremail;
use Illuminate\Http\Request;
use App\Models\Postcomunidad;
use App\Models\Comentariocomunidad;
use Illuminate\Support\Facades\Session;

class PostcomunidadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        
        $rutas=Postcomunidad::paginate(4);
        $usuarios=User::all();           
        return view('plantillas.contenidocomunidad')->with('postcomunidades',$rutas);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Postcomunidad $ruta)
    {
        //        
        $date = date('Y-m-d');        
        $comentariospublicados=Comentariocomunidad::where("postcomunidad_id", "=", $ruta->idpostcomunidad)->get();
        $comentarios= Comentariocomunidad::where("postcomunidad_id", "=", $ruta->idpostcomunidad)->count();
        return view('postcomunidadonly')->with('ruta', $ruta)->with('comentarios',$comentarios)->with('publicaciones',$comentariospublicados)->with('hoy',$date);

    }
    public function show1(Postcomunidad $ruta)
    {
        $date = date('Y-m-d');        
        $comentariospublicados=Comentariocomunidad::where("postcomunidad_id", "=", $ruta->idpostcomunidad)->get();
        $usuarios=User::where("id", "=", $ruta->user_id)->get();
        $comentarios= Comentariocomunidad::where("postcomunidad_id", "=", $ruta->idpostcomunidad)->count();
        return view('postcomunidadonly')->with('ruta', $ruta)->with('comentarios',$comentarios)->with('publicaciones',$comentariospublicados)->with('hoy',$date)->with('usuarios',$usuarios);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($ruta)
    {
        
        $id=$ruta;
        $usuario=Postcomunidad::where("idpostcomunidad", "=", $id )->get();
       
        foreach($usuario as $item){};
       
        $dato=User::where("id","=",$item->user_id)->get();
        foreach($dato as $item2){};
             

        try{
            
            Mail::to($item2->email)->send(new Enviaremail());
        }catch(Exception $e){
            dd($e->getMessage());
        }
        $post=Postcomunidad::where("idpostcomunidad", "=", $id )->delete();
        Session::flash('mensaje', 'Post borrado correctamente.');
        Session::flash('alert-class', 'alert-success');
        return redirect()->route('postcomunidad');
    }
   
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Postrutacabrio;
use App\Models\Comentariorutacabrio;
use Illuminate\Support\Facades\Session;
use App\Http\Requests\StoreComentariorutacabrio;

class ComentariorutacabrioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('contenido');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('contenido')->with('comentario',new Comentariorutacabrio());
        
    }
    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreComentariorutacabrio $request,Postrutacabrio $ruta)
    {
        //
       
        $datosvalidados = $request->validated(); 
        if($request->file('foto')){
            $fichero=$request->file('foto');
            $nuevonombre=time().'_'.$fichero->getClientOriginalName();
            $ruta1=$request->foto->store('','public');
            $datosvalidados['foto']=$ruta1;
        }
        $ruta= $datosvalidados["postrutacabrio_id"];    
        Comentariorutacabrio::create($datosvalidados);
        Session::flash('mensaje','Comentario publicado');
        Session::flash('alert-class','alert-success');
        
       return redirect()->route('contenido',$ruta);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

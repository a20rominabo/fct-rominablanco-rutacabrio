<?php

namespace App\Http\Controllers;
use auth;
use Illuminate\Http\Request;
use App\Models\Postrutacabrio;
use App\Models\Postcomunidad;
use App\Models\Comentariorutacabrio;
use App\Models\Comentariocomunidad;
use App\Models\User;
use App\Http\Requests\StoreRutacabrio;
use App\Http\Requests\StoreComentariorutacabrio;
use App\Http\Requests\StoreUser;
use Illuminate\Support\Facades\Session;
use App\Http\Requests\StorePostRutacabrio;
use App\Http\Requests\StoreUsercambio;
use App\Http\Requests\StorePostComunidad;
class RutacabrioController extends Controller
{

    public function __construct()
    {
        //$this->middleware('auth')->except('create');
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $id=auth()->user()->id;
         return view('plantillasruta.perfil')->with('id',$id);
    }
    public function rutacrear()
    {
        //
         return view('plantillasruta.crearrutacabrio');
    }
    public function crearindex()
    {
        //
         return view('plantillasruta.crear');
    }
    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // 
        return view('plantillasruta.crear')->with('post',new Postrutacabrio());
    }
    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRutacabrio $request)
    {        
        $datosvalidados = $request->validated();
        if($request->file('foto')){
            $fichero=$request->file('foto');
            $nuevonombre=time().'_'.$fichero->getClientOriginalName();
            $ruta1=$request->foto->store('','public');
            $datosvalidados['foto']=$ruta1;
        }
        if($request->file('video')){
            $fichero=$request->file('video');
            $nuevonombre=time().'_'.$fichero->getClientOriginalName();
            $ruta=$request->video->store('','public');
            $datosvalidados['video']=$ruta;
        }      
        $ruta= $datosvalidados["postrutacabrio_id"];    
        Postrutacabrio::create($datosvalidados);
        Session::flash('mensaje','Post editado exitosamente');
        Session::flash('alert-class','alert-success');        
       return redirect()->route('perfilpost');

    }
    
    public function poststore(StorePostComunidad $request)
    {
        $datosvalidados = $request->validated(); 
        if($request->file('foto')){
            $fichero=$request->file('foto');
            $nuevonombre=time().'_'.$fichero->getClientOriginalName();
            $ruta=$request->foto->store('','public');
            $datosvalidados['foto']=$ruta;
        }
        if($request->file('video')){
            $fichero=$request->file('video');
            $nuevonombre=time().'_'.$fichero->getClientOriginalName();
            $ruta=$request->video->store('','public');
            $datosvalidados['video']=$ruta;
        }
    
        
        $datosvalidados['user_id']=auth()->user()->id;
        Postcomunidad::create($datosvalidados);
        Session::flash('mensaje','Post creado exitosamente');
        Session::flash('alert-class','alert-success');
        return redirect()->route('perfilpost');

    }

    public function rutastore(StorePostRutacabrio $request)
    {
        $datosvalidados = $request->validated(); 
        if($request->file('foto')){
            $fichero=$request->file('foto');
            $nuevonombre=time().'_'.$fichero->getClientOriginalName();
            $ruta=$request->foto->store('','public');
            $datosvalidados['foto']=$ruta;
        }
        if($request->file('video')){
            $fichero=$request->file('video');
            $nuevonombre=time().'_'.$fichero->getClientOriginalName();
            $ruta=$request->video->store('','public');
            $datosvalidados['video']=$ruta;
        }
        //  print_r($datosvalidados['foto']);
        //  print_r($datosvalidados['video']);
        // die();
        $datosvalidados['user1_id']=auth()->user()->id;
        Postrutacabrio::create($datosvalidados);
        Session::flash('mensaje','Post creado exitosamente');
        Session::flash('alert-class','alert-success');
        return redirect()->route('perfilruta');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($publicacion)
    {
        $id=$publicacion;
        $post=Postcomunidad::where("idpostcomunidad", "=", $id )->get();
        return view('plantillasruta.listarpostonly')->with('postpublicado',$post);
    }
    public function showruta($publicacion)
    {
        $id=$publicacion;
        $post=Postrutacabrio::where("idpostrutacabrio", "=", $id )->get();
        return view('plantillasruta.listarpostonlyruta')->with('postpublicado',$post);
    }


    public function listarpost()
    {
        $id=auth()->user()->id;
        $postpublicados=Postcomunidad::where("user_id", "=", $id )->orderBy('fecha', 'desc')->get();
        return view('plantillasruta.listarpost')->with('postpublicados',$postpublicados);
        
    }
    public function listarpostcomunidad()
    {
        
        $postpublicados=Postcomunidad::all();
        return view('plantillasruta.listarpostcomunidad')->with('postpublicados',$postpublicados);
        
    }
    
    public function listarruta()
    {
        $id=auth()->user()->id;
        $postpublicados=Postrutacabrio::where("user1_id", "=", $id )->orderBy('fecha', 'desc')->get();
        return view('plantillasruta.listarruta')->with('postpublicados',$postpublicados);
        
    }
    public function listarusuarios()
    {
       
        $postpublicados=User::all();
        return view('plantillasruta.listarusuarios')->with('postpublicados',$postpublicados);
        
    }
   
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit( $publicacion)
    {
       $id=$publicacion;
       $post=Postcomunidad::where("idpostcomunidad", "=", $id )->get();
       return view('plantillasruta.editar')->with('publicacion', $post);

    }
    public function perfiledit($id)
    {
        $usuario=auth()->user();
        return view('plantillasruta.editarperfil')->with('usuario', $usuario)->with('id',$id);
    }
    public function editruta( $publicacion)
    {
       $id=$publicacion;
       $post=Postrutacabrio::where("idpostrutacabrio", "=", $id )->get();
       return view('plantillasruta.editarruta')->with('publicacion', $post);

    }
    public function editarusuarios( $publicacion)
    {
       
       $id=$publicacion;
       $post=User::where("id", "=", $id )->get();
       return view('plantillasruta.editarusuario')->with('publicacion', $post);

    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StorePostComunidad $request, $id)
    {
        $datosvalidados = $request->validated();  
        if($request->file('foto')){
            $fichero=$request->file('foto');
            $nuevonombre=time().'_'.$fichero->getClientOriginalName();
            $ruta=$request->foto->store('','public');
            $datosvalidados['foto']=$ruta;
        }
        if($request->file('video')){
            $fichero=$request->file('video');
            $nuevonombre=time().'_'.$fichero->getClientOriginalName();
            $ruta=$request->video->store('','public');
            $datosvalidados['video']=$ruta;
        } 
        Postcomunidad::where("idpostcomunidad", "=", $id )->update($datosvalidados);
        Session::flash('mensaje','Post editado exitosamente');
        Session::flash('alert-class','alert-success');
        return redirect()->route('rutacabrio.show',$id);

    }

    public function perfilup(StoreUser $request,$idp)
    {
        $datosvalidados = $request->validated(); 
       User::where("id", "=", $idp )->update($datosvalidados);
        Session::flash('mensaje','Perfil editado exitosamente');
        Session::flash('alert-class','alert-success');
        return redirect()->route('perfil');
    }

    public function updateruta(StorePostRutacabrio $request, $id)
    {
        $datosvalidados = $request->validated();
        if($request->file('foto')){
            $fichero=$request->file('foto');
            $nuevonombre=time().'_'.$fichero->getClientOriginalName();
            $ruta=$request->foto->store('','public');
            $datosvalidados['foto']=$ruta;
        }
        if($request->file('video')){
            $fichero=$request->file('video');
            $nuevonombre=time().'_'.$fichero->getClientOriginalName();
            $ruta=$request->video->store('','public');
            $datosvalidados['video']=$ruta;
        }   
        Postrutacabrio::where("idpostrutacabrio", "=", $id )->update($datosvalidados);
        Session::flash('mensaje','Post editado exitosamente');
        Session::flash('alert-class','alert-success');
        return redirect()->route('mostrarruta',$id);

    }
    public function updateusuarios(StoreUsercambio $request, $id)
    {
        $datosvalidados = $request->validated();
        
        User::where("id", "=", $id )->update($datosvalidados);
        Session::flash('mensaje','Usuario con categoria editada exitosamente');
        Session::flash('alert-class','alert-success');
        return redirect()->route('listarusuarios');

    }
    

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($publicacion)
    {
        $id=$publicacion;
        $post=Postcomunidad::where("idpostcomunidad", "=", $id )->delete();
        Session::flash('mensaje', 'Post borrado correctamente.');
        Session::flash('alert-class', 'alert-success');
        if((auth()->user()->categoria)=== 'usuariorutacabrio'){
            return redirect()->route('perfilpostcomunidad');
        }else{
            return redirect()->route('perfilpost');
        }
        

    }
    public function destroyruta($publicacion)
    {
        $id=$publicacion;
        $post=Postrutacabrio::where("idpostrutacabrio", "=", $id )->delete();
        Session::flash('mensaje', 'Post borrado correctamente.');
        Session::flash('alert-class', 'alert-success');
        return redirect()->route('perfilruta');
    }
    public function destroyusuarios($publicacion)
    {
        $id=$publicacion;
        $post=User::where("id", "=", $id )->delete();
        Session::flash('mensaje', 'Usuario borrado correctamente.');
        Session::flash('alert-class', 'alert-success');     
       
        return redirect()->route('plantillasruta.listarusuarios');
    }
   
   
   
}

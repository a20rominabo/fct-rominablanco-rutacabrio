<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Comentariorutacabrio;
use App\Models\Comentariocomunidad;

use App\Http\Requests\StoreComentarioperfilrutacabrio;
use App\Http\Requests\StoreUser;
use Illuminate\Support\Facades\Session;
class ComentarioPerfilController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        //$this->middleware('auth')->except('create');
        $this->middleware('auth');
    }

    public function index()
    {
        //
        $id=auth()->user()->id;
        $postpublicados=Comentariorutacabrio::where("user3_id", "=", $id )->orderBy('fecha', 'desc')->get();
        $postcomunidades=Comentariocomunidad::where("user2_id","=",$id)->orderBy('fecha','desc')->get();
        return view('plantillasruta.listarcomentario')->with('postpublicados',$postpublicados)->with('postcomunidades',$postcomunidades);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        //return view('plantillasruta.editarcomentruta')->with('publicacion',new Comentariorutacabrio());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store( $request)
    {
        }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($publicacion)
    {
        //        
        $id=$publicacion;
        $post=Comentariorutacabrio::where("idcomentariorutacabrio", "=", $id )->get();
        return view('plantillasruta.listarcomentarioonly')->with('postpublicado',$post);
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($publicacion)
    {        
     
        $id=$publicacion;
        $post=Comentariorutacabrio::where("idcomentariorutacabrio", "=", $id )->get();
        return view('plantillasruta.editarcomentruta')->with('publicacion', $post);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreComentarioperfilrutacabrio $request,$id)
    {
        //
       
        $datosvalidados = $request->validated();  
        if($request->file('foto')){
            $fichero=$request->file('foto');
            $nuevonombre=time().'_'.$fichero->getClientOriginalName();
            $ruta=$request->foto->store('','public');
            $datosvalidados['foto']=$ruta;
        }
        Comentariorutacabrio::where("idcomentariorutacabrio", "=", $id )->update($datosvalidados);
        Session::flash('mensaje','Comentario modificado exitosamente');
        Session::flash('alert-class','alert-success');
       
        return redirect()->route('comentarioperfil.show',$id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($publicacion)
    {
        //
        $id=$publicacion;
        $post=Comentariorutacabrio::where("idcomentariorutacabrio", "=", $id )->delete();
        Session::flash('mensaje', 'Comentario borrado correctamente.');
        Session::flash('alert-class', 'alert-success');
        return redirect()->route('perfilcomentario');
    }
}

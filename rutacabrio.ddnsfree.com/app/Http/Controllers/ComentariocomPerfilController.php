<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Comentariocomunidad;

use App\Http\Requests\StoreComentarioperfilcomunidad;
use App\Http\Requests\StoreUser;
use Illuminate\Support\Facades\Session;
class ComentariocomPerfilController extends Controller
{

    public function __construct()
    {
        //$this->middleware('auth')->except('create');
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($publicacion)
    {
        //
        $id=$publicacion;
        $post=Comentariocomunidad::where("idcomentariocomunidad", "=", $id )->get();
        return view('plantillasruta.listarcomentarioonly')->with('postpublicado',$post);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($publicacion)
    {
        //
        $id=$publicacion;
        $post=Comentariocomunidad::where("idcomentariocomunidad", "=", $id )->get();
        return view('plantillasruta.editarcomentcomunidad')->with('publicacion', $post);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreComentarioperfilcomunidad $request,$id)
    {
        //
        $datosvalidados = $request->validated();  
        if($request->file('foto')){
            $fichero=$request->file('foto');
            $nuevonombre=time().'_'.$fichero->getClientOriginalName();
            $ruta=$request->foto->store('','public');
            $datosvalidados['foto']=$ruta;
        }
        Comentariocomunidad::where("idcomentariocomunidad", "=", $id )->update($datosvalidados);
        Session::flash('mensaje','Comentario modificado exitosamente');
        Session::flash('alert-class','alert-success');
       
        return redirect()->route('comentarioperfilcomunidad.show',$id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($publicacion)
    {
        //
        $id=$publicacion;
        $post=Comentariocomunidad::where("idcomentariocomunidad", "=", $id )->delete();
        Session::flash('mensaje', 'Comentario borrado correctamente.');
        Session::flash('alert-class', 'alert-success');
        return redirect()->route('perfilcomentario');
    }
}

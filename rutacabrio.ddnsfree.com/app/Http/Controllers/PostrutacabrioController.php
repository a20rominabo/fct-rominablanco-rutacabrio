<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Postrutacabrio;
use App\Models\Comentariorutacabrio;
use Illuminate\Support\Facades\Session;

class PostrutacabrioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $rutas=Postrutacabrio::orderBy('fecha', 'desc')->paginate(4);       
        return view('rutacabrio')->with('postrutacabrios',$rutas);
    }
    public function show2021()
    {
        //
        $from= date('2018-01-01');
        $to= date('2018-12-31');
        // ->orderBy('fecha', 'desc')
        $rutas=Postrutacabrio::whereBetween('fecha',array($from,$to))->orderBy('fecha', 'desc')->paginate(4);       
        return view('rutacabrio')->with('postrutacabrios',$rutas);
    }
    public function show2022()
    {
        //
        $from= date('2017-01-01');
        $to= date('2017-12-31');
        // ->orderBy('fecha', 'desc')
        $rutas=Postrutacabrio::whereBetween('fecha',array($from,$to))->orderBy('fecha', 'desc')->paginate(4);       
        return view('rutacabrio')->with('postrutacabrios',$rutas);
    }
    public function show2020()
    {
        //
        $from= date('2020-01-01');
        $to= date('2020-12-31');
        // ->orderBy('fecha', 'desc')
        $rutas=Postrutacabrio::whereBetween('fecha',array($from,$to))->orderBy('fecha', 'desc')->paginate(4);       
        return view('rutacabrio')->with('postrutacabrios',$rutas);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Postrutacabrio $ruta)
    {
        //         
        $date = date('Y-m-d');
        $comentariospublicados=Comentariorutacabrio::where("postrutacabrio_id", "=", $ruta->idpostrutacabrio)->get();
        $comentarios= Comentariorutacabrio::where("postrutacabrio_id", "=", $ruta->idpostrutacabrio)->count();
        return view('contenidorutacabrio')->with('ruta', $ruta)->with('comentarios',$comentarios)->with('publicaciones',$comentariospublicados)->with('hoy',$date);

    }
    public function showcoment(Postrutacabrio $ruta)
    {
        //         
        $date = date('Y-m-d');        
        $comentariospublicados=Comentariorutacabrio::where("postrutacabrio_id", "=", $ruta->idpostrutacabrio)->get();
        $comentarios= Comentariorutacabrio::where("postrutacabrio_id", "=", $ruta->idpostrutacabrio)->count();
        
        return view('contenidorutacabrio')->with('ruta', $ruta)->with('comentarios',$comentarios)->with('publicaciones',$comentariospublicados)->with('hoy',$date);

    }
    

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    
    
}

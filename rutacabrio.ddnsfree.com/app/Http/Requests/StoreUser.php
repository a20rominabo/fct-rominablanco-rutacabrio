<?php

namespace App\Http\Requests;
use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;

class StoreUser extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'nombre'=>'nullable|max:255',
            'apellidos'=>'nullable|max:255',
            'login'=>'nullable|max:250',
            'telefono'=>'nullable|max:255',
            'tipoCoche'=>'nullable|max:250',
            'email'=>'nullable|max:250',
            'password'=>'nullable|max:250'
        ];
    }
}

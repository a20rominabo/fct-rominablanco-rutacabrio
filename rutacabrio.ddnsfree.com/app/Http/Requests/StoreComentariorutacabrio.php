<?php

namespace App\Http\Requests;

use App\Models\Comentariorutacabrio;
use Illuminate\Foundation\Http\FormRequest;

class StoreComentariorutacabrio extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

            //protected $primaryKey = 'idcomentariorutacabrio';
            //protected $fillable = array('nick','descripcion','fecha','foto');
             'postrutacabrio_id'=>'required|max:255',
             'user3_id'=>'required|max:255',
              'nick'=>'nullable|max:255',
            'descripcion'=>'required|max:1050',
            'fecha'=>'required|date',
            'foto'=>'nullable|max:255'
        ];
    }
}

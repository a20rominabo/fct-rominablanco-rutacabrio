<?php

namespace App\Http\Requests;

use App\Models\Comentariorutacabrio;
use Illuminate\Foundation\Http\FormRequest;

class StoreComentarioperfilrutacabrio extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

            //protected $primaryKey = 'idcomentariorutacabrio';
            //protected $fillable = array('nick','descripcion','fecha','foto');
            
            'descripcion'=>'required|max:1050',
            'fecha'=>'required|date',
            'foto'=>'nullable|max:255'
        ];
    }
}

<?php

namespace App\Http\Requests;
use App\Models\Postcomunidad;
use Illuminate\Foundation\Http\FormRequest;

class StorePostComunidad extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'titulo'=>'required|max:255',
            'tipoRuta'=>'nullable|max:255',
            'foto'=>'nullable|mimes:png,jpg,jpeg|max:2048',
            'video'=>'nullable|mimes:mp4,webm,ogv,ogg|max:102400',
            'enlacesExterno'=>'nullable|max:500',
            'coordenadaLatitud'=>'nullable|max:255',
            'coordenadaLongitud'=>'nullable|max:255',
            'descripcion'=>'nullable|max:1050',
            'fecha'=>'nullable|date',
            'comunidadAutonoma'=>'nullable|max:255',
            'ciudad'=>'nullable|max:255',
            

        ];
    }
}

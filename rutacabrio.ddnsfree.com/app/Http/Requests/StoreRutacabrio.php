<?php

namespace App\Http\Requests;
use App\Models\Postrutacabrio;
use Illuminate\Foundation\Http\FormRequest;

class StoreRutacabrio extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'titulo'=>'required|max:255',
            'tipoRuta'=>'nullable|max:255',
            'foto'=>'nullable|mimes:png,jpg,jpeg|max:2048',
            'video'=>'nullable|mimes:mp4,ogx,oga,ogv,ogg,webm|max:9000000',
            'enlaceInstagram'=>'nullable|max:255',            
            'enlaceYoutube'=>'nullable|max:255',
            'coordenadaLatitud'=>'nullable|max:255',
            'coordenadaLongitud'=>'nullable|max:255',
            'descripcion'=>'nullable|max:1050',
            'fecha'=>'nullable|date',
            'comunidadAutonoma'=>'nullable|max:255',
            'ciudad'=>'nullable|max:255',
            'enlacesExternos'=>'nullable|max:500'


        ];
        
    }
}

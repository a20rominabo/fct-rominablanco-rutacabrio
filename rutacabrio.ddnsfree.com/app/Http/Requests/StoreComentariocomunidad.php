<?php

namespace App\Http\Requests;
use App\Models\Postcomunidad;
use Illuminate\Foundation\Http\FormRequest;

class StoreComentariocomunidad extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'postcomunidad_id'=>'required|max:255',
            'nick'=>'required|max:255',
            'descripcion'=>'required|max:1050',
            'fecha'=>'required|date',
            'foto'=>'nullable|max:255'
        ];
    }
}

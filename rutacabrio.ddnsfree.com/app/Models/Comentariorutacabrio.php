<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Comentariorutacabrio extends Model
{
    use HasFactory;
    protected $primaryKey = 'idcomentariorutacabrio';
    protected $fillable = array('user3_id','postrutacabrio_id','nick','descripcion','fecha','foto');
    public $timestamps=false;
    
}

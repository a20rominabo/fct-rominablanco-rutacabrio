<?php

namespace App\Models;

use App\Models\Postcomunidad;
use App\Models\Postrutacabrio;
use Laravel\Sanctum\HasApiTokens;
use App\Models\Comentariocomunidad;
use App\Models\Comentariorutacabrio;
use Laravel\Jetstream\HasProfilePhoto;
use Illuminate\Notifications\Notifiable;
use Laravel\Fortify\TwoFactorAuthenticatable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasApiTokens;
    use HasFactory;
    use HasProfilePhoto;
    use Notifiable;
    use TwoFactorAuthenticatable;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'nombre',
        'apellidos',
        'login',
        'categoria',
        'telefono',
        'tipoCoche',
        'email',
        'password',      
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'two_factor_recovery_codes',
        'two_factor_secret',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'profile_photo_url',
    ];

    public function postscomunidad(){
        return $this->hasMany(Postcomunidad::class,'user_id','id');
    }
    public function postsrutas(){
        return $this->hasMany(Postrutacabrio::class,'user1_id','id');
    }
    public function comentcomunidad(){
        return $this->hasMany(Comentariocomunidad::class,'user2_id','id');
    }
    public function comentrutas(){
        return $this->hasMany(Comentariorutacabrio::class,'user3_id','id');
    }



}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Postcomunidad extends Model
{
    use HasFactory;
    protected $primaryKey = 'idpostcomunidad';
    protected $fillable = array('user_id','titulo','tipoRuta','foto','video',
    'enlaceExterno','coordenadaLatitud','coordenadaLongitud',
    'descripcion','fecha','comunidadAutonoma','ciudad','publicado');
    
    public $timestamps=false;
    
    public function comentarioscomunidad(){
       
        return $this->hasMany(Comentariorutacabrio::class,'postcomunidad_id','idpostcomunidad');
    }
}

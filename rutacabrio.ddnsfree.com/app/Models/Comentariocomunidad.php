<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Comentariocomunidad extends Model
{
    use HasFactory;
    protected $primaryKey = 'idcomentariocomunidad';
    protected $fillable = array('user2_id','postcomunidad_id','nick','descripcion','fecha','foto');
    public $timestamps=false;
}

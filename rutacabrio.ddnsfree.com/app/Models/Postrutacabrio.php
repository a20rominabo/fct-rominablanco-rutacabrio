<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Postrutacabrio extends Model
{
    use HasFactory;
    protected $primaryKey = 'idpostrutacabrio';
    protected $fillable = array('user1_id','titulo', 'tipoRuta','foto','video','enlaceInstagram',
    'enlaceYoutube','coordenadaLatitud','coordenadaLongitud','descripcion','fecha',
    'comunidadAutonoma','ciudad','enlacesExternos');
    public $timestamps=false;
    public function comentariosrutas(){

        return $this->hasMany(Comentariorutacabrio::class,'postrutacabrio_id','idpostrutacabrio');
    }
}

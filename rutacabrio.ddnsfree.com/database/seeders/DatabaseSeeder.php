<?php

namespace Database\Seeders;

use App\Models\Postcomunidad;
use App\Models\Postrutacabrio;
use Illuminate\Database\Seeder;
use App\Models\Comentariocomunidad;
use App\Models\Comentariorutacabrio;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        \App\Models\User::factory(10)->create();
        Postrutacabrio::factory(10)->create();
        Postcomunidad::factory(10)->create();
        Comentariocomunidad::factory(10)->create();
        Comentariorutacabrio::factory(10)->create();
        
    }
}

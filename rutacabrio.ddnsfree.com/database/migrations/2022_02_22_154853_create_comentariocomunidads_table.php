<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateComentariocomunidadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comentariocomunidads', function (Blueprint $table) {
            $table->bigIncrements('idcomentariocomunidad');
            $table->unsignedBigInteger('user2_id');
            $table->unsignedBigInteger('postcomunidad_id')->nullable();
            $table->string('nick',255);
            $table->string('descripcion',1050)->nullable();
            $table->date('fecha');
            $table->string('foto')->nullable();
            $table->foreign('user2_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('postcomunidad_id')->references('idpostcomunidad')->on('postcomunidads')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comentariocomunidads');
    }
}

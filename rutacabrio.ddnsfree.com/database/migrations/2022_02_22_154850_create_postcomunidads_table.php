<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostcomunidadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('postcomunidads', function (Blueprint $table) {
            $table->id('idpostcomunidad')->autoIncrement();
            $table->unsignedBigInteger('user_id')->nullable();
            $table->string('titulo',255);
            $table->string('tipoRuta',255)->nullable();
            $table->string('foto')->nullable();
            $table->string('video')->nullable();
            $table->string('enlacesExterno',500)->nullable();
            $table->string('coordenadaLatitud',255)->nullable();
            $table->string('coordenadaLongitud',255)->nullable();
            $table->string('descripcion',1050)->nullable();
            $table->date('fecha');
            $table->string('comunidadAutonoma',255)->nullable();
            $table->string('ciudad',255)->nullable();
            $table->string('publicado',255)->nullable();            
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

            // Esta es la otra opcion de añadir los timestamps:
           // $table->timestamps();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('postcomunidads');
    }
}

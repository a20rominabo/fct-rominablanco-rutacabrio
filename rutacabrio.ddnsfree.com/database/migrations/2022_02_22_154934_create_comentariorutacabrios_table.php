<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateComentariorutacabriosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comentariorutacabrios', function (Blueprint $table) {
            $table->id('idcomentariorutacabrio')->autoIncrement();
            $table->unsignedBigInteger('user3_id')->nullable();
            $table->unsignedBigInteger('postrutacabrio_id')->nullable();
            $table->string('nick',255);
            $table->string('descripcion',1050)->nullable();
            $table->date('fecha');
            $table->string('foto')->nullable();
            $table->foreign('postrutacabrio_id')->references('idpostrutacabrio')->on('postrutacabrios')->onDelete('cascade');
            $table->foreign('user3_id')->references('id')->on('users')->onDelete('cascade');      
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comentariorutacabrios');
    }
}

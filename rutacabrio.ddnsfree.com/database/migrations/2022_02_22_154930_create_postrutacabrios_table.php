<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostrutacabriosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('postrutacabrios', function (Blueprint $table) {
            $table->id('idpostrutacabrio')->autoIncrement();
            $table->unsignedBigInteger('user1_id')->nullable();
            $table->string('titulo',255);
            $table->string('tipoRuta',255)->nullable();
            $table->string('foto')->nullable();
            $table->string('video')->nullable();
            $table->string('enlaceInstagram',255)->nullable();
            $table->string('enlaceYoutube',255)->nullable();
            $table->string('coordenadaLatitud',255)->nullable();
            $table->string('coordenadaLongitud',255)->nullable();
            $table->string('descripcion',1050)->nullable();
            $table->date('fecha');
            $table->string('comunidadAutonoma',255)->nullable();
            $table->string('ciudad',255)->nullable();
            $table->string('enlacesExternos',500)->nullable();
            $table->foreign('user1_id')->references('id')->on('users')->onDelete('cascade');            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('postrutacabrios');
    }
}

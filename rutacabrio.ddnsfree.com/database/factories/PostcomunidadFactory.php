<?php

namespace Database\Factories;

use App\Models\User;
use App\Models\Postcomunidad;
use Illuminate\Database\Eloquent\Factories\Factory;

class PostcomunidadFactory extends Factory
{

    protected $model=Postcomunidad::class;
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $totalUsuarios=User::all()->count();
        return [
            //
            'user_id'=>$this->faker->numberBetween(1,$totalUsuarios),
            'titulo'=>$this->faker->company(),
            'tipoRuta'=>$this->faker->company(),
            'foto'=>$this->faker->image('storage/app/public',400,300, null, false),
            // 'video'=>$this->faker->file_path(depth:1, category:'video', extension:'mp4'),
            'enlacesExterno'=>$this->faker->url(),
            'coordenadaLatitud'=>$this->faker->latitude(),
            'coordenadaLongitud'=>$this->faker->longitude(),
            'descripcion'=>$this->faker->sentence(),
            'fecha'=>$this->faker->date(),
            'comunidadAutonoma'=>$this->faker->country(),
            'ciudad'=>$this->faker->city(),
            'publicado'=>$this->faker->boolean()
        ];
    }
}

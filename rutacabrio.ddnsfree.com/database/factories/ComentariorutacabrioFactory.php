<?php

namespace Database\Factories;

use App\Models\User;
use App\Models\Postrutacabrio;
use App\Models\Comentariorutacabrio;
use Illuminate\Database\Eloquent\Factories\Factory;

class ComentariorutacabrioFactory extends Factory
{
    protected $model=Comentariorutacabrio::class;
  
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        // public/storage/images
        $totalUsuarios=User::all()->count();
    $totalPost=Postrutacabrio::all()->count();
        return [
            //
            'user3_id'=>$this->faker->numberBetween(1,$totalUsuarios),
            'postrutacabrio_id'=>$this->faker->numberBetween(1,$totalPost),
            'nick'=>$this->faker->lastName(),
            'descripcion'=>$this->faker->paragraph(),
            'fecha'=>$this->faker->date(),
            'foto'=>$this->faker->image('storage/app/public',400,300, null, false)
        ];
    }
}

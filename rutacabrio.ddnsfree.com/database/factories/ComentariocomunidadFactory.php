<?php

namespace Database\Factories;

use App\Models\User;
use App\Models\Postcomunidad;
use App\Models\Comentariocomunidad;
use Illuminate\Database\Eloquent\Factories\Factory;

class ComentariocomunidadFactory extends Factory
{
    protected $model=Comentariocomunidad::class;
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $totalUsuarios=User::all()->count();
        $totalPost=Postcomunidad::all()->count();
        return [
            //
            'user2_id'=>$this->faker->numberBetween(1,$totalUsuarios),
            'postcomunidad_id'=>$this->faker->numberBetween(1,$totalPost),
            'nick'=>$this->faker->lastName(),
            'descripcion'=>$this->faker->paragraph(),
            'fecha'=>$this->faker->date(),
            'foto'=>$this->faker->image('storage/app/public',400,300, null, false),
        ];
    }
}

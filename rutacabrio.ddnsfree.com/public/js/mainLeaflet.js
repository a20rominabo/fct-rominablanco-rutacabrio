const tilesProvider = 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';

let valorLatitud=$('#latitud').attr('data-value');
let valorLongitud=$('#longitud').attr('data-value');
let titulo=$('#map').attr('data-value');
let iconoFlecha = L.icon({
    iconUrl: "/images/alfiler.png",
    iconSize: [50, 50],
    iconAnchor: [15, 40]

});
// 42.878, -8.537
let miMapa = L.map('map').setView([valorLatitud, valorLongitud], 7);
L.tileLayer(tilesProvider, {
    maxZoom:45,
}).addTo(miMapa);
let marcador = L.marker([valorLatitud,valorLongitud], { title: "Ruta : "+ titulo ,
    icon: iconoFlecha }).addTo(miMapa);
marcador.bindPopup("Ruta : "+ titulo);


